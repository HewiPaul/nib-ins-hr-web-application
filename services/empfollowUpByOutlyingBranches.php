<?php
    require_once 'db.php';
    $data = json_decode(file_get_contents("php://input"));
    $branch = $data->branch;
	$count_MA_Male = $conn->query("SELECT Count(id) as MaleMA FROM employees WHERE sex='Male' AND 	Branch='$branch' AND (educational_level='MA.' OR educational_level='Msc.') AND `Status` = '1'") or die(mysqli_error());
	$count_MA_Female = $conn->query("SELECT Count(id) as FemaleMA FROM employees WHERE sex='Female' AND Branch='$branch' AND (educational_level='MA.' OR educational_level='Msc.') AND `Status` = '1'") or die(mysqli_error());
	$count_BA_Male = $conn->query("SELECT Count(id) as MaleBA FROM employees WHERE sex='Male' AND Branch='$branch' AND (educational_level='BA.' OR educational_level='Bsc.') AND `Status` = '1'") or die(mysqli_error());
	$count_BA_Female = $conn->query("SELECT Count(id) as FemaleBA FROM employees WHERE sex='Female' AND	Branch='$branch' AND (educational_level='BA.' OR educational_level='Bsc.') AND `Status` = '1'") or die(mysqli_error());
	$count_Diploma_Male = $conn->query("SELECT Count(id) as MaleDip FROM employees WHERE sex='Male' AND	Branch='$branch' AND educational_level='Diploma' AND `Status` = '1'") or die(mysqli_error());
	$count_Diploma_Female = $conn->query("SELECT Count(id) as FemaleDip FROM employees WHERE sex='Female' AND Branch='$branch' AND educational_level='Diploma' AND `Status` = '1'") or die(mysqli_error());
	$count_Cert_Male = $conn->query("SELECT Count(id) as MaleCert FROM employees WHERE sex='Male' AND Branch='$branch' AND educational_level='Certificate' AND `Status` = '1'") or die(mysqli_error());
	$count_Cert_Female = $conn->query("SELECT Count(id) as FemaleCert FROM employees WHERE sex='Female' AND	Branch='$branch' AND educational_level='Certificate' AND `Status` = '1'") or die(mysqli_error());
	
	$data1 = $count_MA_Male->fetch_array();
	$data2 = $count_MA_Female->fetch_array();
	$data3 = $count_BA_Male->fetch_array();
	$data4 = $count_BA_Female->fetch_array();
	$data5 = $count_Diploma_Male->fetch_array();
	$data6 = $count_Diploma_Female->fetch_array();
	$data7 = $count_Cert_Male->fetch_array();
	$data8 = $count_Cert_Female->fetch_array();
	
	$data = array();
	$data = [ $branch , $data1[0] , $data2[0], $data3[0], $data4[0], $data5[0] , $data6[0], $data7[0], $data8[0]];
	echo json_encode($data);
?>