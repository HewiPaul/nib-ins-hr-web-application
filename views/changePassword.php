<?php
include("../services/auth.php");
?>
<link rel="stylesheet" href="dist/css/select2.css">
<div width="100%" class="col-lg-12" ng-controller="userController">
    <div class="panel panel-default">
            <div class="panel-heading">
                  Change Password
            </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
                <form enctype="multipart/form-data" role="form" name="form"> 
                <div class="form-group">
                  <label class="control-label" for="inputError">Username</label>
                  <input id="uname" type="text" class="form-control" value="<?php echo $_SESSION['username']; ?>" readonly="true"/>
                </div>
                <div class="form-group">
                        <label class="control-label" for="inputError">Old Password</label>
                      <input id="oldpass" type="password" class="form-control" placeholder="Old Password" ng-model="oldpassword" required>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="inputError">New Password</label>
                      <input id="newpass" type="password" class="form-control" placeholder="NewPassword" ng-model="newPassword" required>
                    </div> 
                    <div class="form-group">
                            <label class="control-label" for="inputError">Confirm New Password</label>
                          <input id="Cnewpass" type="password" class="form-control" placeholder="Confirm New Password" ng-model="CnewPassword" ng-pattern="newPassword" required />
                           <span ng-show="newPassword != CnewPassword">Your passwords must match.</span>
                    </div>
                <input style="margin: 5px; width: 140px" class="btn btn-success" value="Change Password" type="submit" ng-click="form.$valid && changePassword()"/>
                </form>
        </div>
    </div>
                        
                        <?php
                        if($_SESSION['isAdmin']){
                        ?>
    <div class="panel panel-default">
            <div class="panel-heading">
                  Create New HR User
            </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
                <form enctype="multipart/form-data" role="form" name="form"> 
                <div class="form-group">
                  <label class="control-label" for="inputError">User Name</label>
                  <input id="username" type="text" class="form-control" placeholder="User Name" required/>
                </div>
                    <div class="form-group">
                        <label class="control-label" for="inputError">New Password</label>
                      <input id="newpasss" type="password" class="form-control" placeholder="New Password" ng-model="newPwd" required/>
                    </div> 
                    <div class="form-group">
                            <label class="control-label" for="inputError">Confirm New Password</label>
                          <input id="Cnewpasss" type="password" class="form-control" placeholder="Confirm New Password" ng-model="CnewPasssword" ng-pattern="newPwd" required />
                           <span ng-show="newPwd != CnewPasssword">Your passwords must match.</span>
                    </div>
                      <div style="padding-top: 5px" class="form-group">
                                                      <label>Admin.</label>
                                                      <select id="admin" class="js-example-basic-single form-control"
                                                            name="state" ng-model="admin" class="form-control">
                                                            <option value="" selected disabled hidden>Choose here</option>
                                                            <option value="1">True</option>
                                                            <option value="0">False</option>
                                                      </select>
                        </div>
                <input style="margin: 5px; width: 140px" class="btn btn-success" value="Create User" type="submit" ng-click="form.$valid && createUser()"/>
                </form>
        </div>
    </div>

     <div class="panel panel-default">
            <div class="panel-heading">
                  Create New HR User
            </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
                <table  cellspacing="0" width="100%" class="display table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
                        <thead>
                            <tr>
                             <td>Id</td>
                             <td>User Name</td>
                             <td>Is Admin</td>
                             <td>Reset</td>
                             <td>Delete</td>
                       </tr>
                     </thead>
                     <tbody>
                         <tr ng-repeat="row in hrusers">
                             <td>{{row.Id}}</td>
                             <td>{{row.username}}</td>
                             <td>{{row.isAdmin == 1 ? 'True' : 'False'}}</td>
                             <td style="white-space: nowrap">
                                <a class="btn btn-warning" type="button" name="reset" ng-click="resetPwd(row.Id)"><i class="fa fa-cog"></i></a>
                             </td>
                              <td style="white-space: nowrap">
                                <a class="btn btn-danger" type="button" name="reset" ng-click="deleteUser(row.Id)"><i class="fa fa-trash-alt"></i></a>
                             </td>
                         </tr>
                     </tbody>
                </table>
        </div>
    </div>

                        <?php
                        }
                        ?>
</div>

<script src="scripts/select2.min.js"></script>
<script>
// In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
    $('.js-example-basic-single').select2();
});
</script>