-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Dec 19, 2018 at 03:37 PM
-- Server version: 5.7.24-0ubuntu0.18.04.1
-- PHP Version: 7.2.10-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hrmanagmentdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

CREATE TABLE `branches` (
  `Id` int(11) NOT NULL,
  `BranchName` varchar(500) NOT NULL,
  `BranchType` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`Id`, `BranchName`, `BranchType`) VALUES
(46, 'Head Office', 'Addis Ababa'),
(2, 'Ayer Tena', 'Addis Ababa'),
(3, 'Tiret', 'Addis Ababa'),
(4, 'Ras', 'Addis Ababa'),
(17, 'Piassa', 'Addis Ababa'),
(8, 'Sefere Selam', 'Addis Ababa'),
(9, 'Adama', 'Outlying'),
(10, 'Jimma', 'Outlying'),
(36, 'Dessie', 'Outlying'),
(30, 'Bole', 'Addis Ababa'),
(13, 'Megenagna', 'Addis Ababa'),
(14, 'Genet', 'Addis Ababa'),
(16, 'Tana', 'Addis Ababa'),
(18, 'Urael', 'Addis Ababa'),
(20, 'Addis Ketema', 'Addis Ababa'),
(21, 'Gotera', 'Addis Ababa'),
(23, 'Meshualekia', 'Addis Ababa'),
(24, 'Ledeta', 'Addis Ababa'),
(25, 'Lebu', 'Addis Ababa'),
(26, 'Gurd Shola', 'Addis Ababa'),
(27, 'Gerji', 'Addis Ababa'),
(28, 'Kera', 'Addis Ababa'),
(29, 'Tele Medhanialem', 'Addis Ababa'),
(31, 'Arat Kilo', 'Addis Ababa'),
(32, 'Bisrate Gebriel', 'Addis Ababa'),
(33, 'Kality', 'Addis Ababa'),
(34, 'Haya Arat Akababi', 'Addis Ababa'),
(35, 'Dire Dawa', 'Outlying'),
(37, 'Gonder', 'Outlying'),
(38, 'Mekelle', 'Outlying'),
(39, 'Awassa', 'Outlying'),
(40, 'Bahir Dar', 'Outlying'),
(41, 'Hossaena', 'Outlying'),
(42, 'Modjo', 'Outlying'),
(43, 'Debre Brehan', 'Outlying'),
(44, 'Alemgena', 'Outlying'),
(45, 'Wolikitie', 'Outlying');

-- --------------------------------------------------------

--
-- Table structure for table `certefications`
--

CREATE TABLE `certefications` (
  `Id` int(11) NOT NULL,
  `CerteficationName` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `GivenBy` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `certefications`
--

INSERT INTO `certefications` (`Id`, `CerteficationName`, `GivenBy`) VALUES
(4, 'Certified Public Accountant', 'CPA'),
(5, 'Certified Financial Analyst', 'CFA'),
(6, 'Certified Management Accountant ', 'CMA'),
(7, 'Enrolled Agent', 'EA'),
(8, 'Certified Internal Auditor', 'CIA'),
(9, 'Certified Information Systems Auditor', 'CISA'),
(10, 'Certified Fraud Examiner', 'CFE'),
(11, 'Certified Government Auditing Professional', 'CGAP'),
(12, 'Certified Bank Auditor', 'CBA'),
(13, 'Chartered Alternative Investment Analyst', 'CAIA'),
(15, 'Cisco Certified Network Professional (CCNP)', 'CISCO'),
(16, 'Cisco Certified Network Expert (CCIE)', 'CISCO'),
(20, 'COC Level 1 (Information Technology Support Service)', 'OCACC'),
(21, 'COC Level 2 (Information Technology Support Service)', 'OCACC'),
(22, 'COC Level 3 (Database Management and Support)', 'OCACC'),
(23, 'COC Level 4 (Database Management and Support)', 'OCACC'),
(24, 'COC Level 3 (Network Administration and Support)', 'OCACC'),
(25, 'COC Level 4 (Network Administration and Support)', 'OCACC'),
(26, 'COC Level 3 (Web Development and Multimedia)', 'OCACC'),
(27, 'COC Level 4 (Web Development and Multimedia)', 'OCACC'),
(29, 'COC Level 5 (Project Management)', 'OCACC');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `Id` int(11) NOT NULL,
  `DepartmentName` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`Id`, `DepartmentName`) VALUES
(1, 'General Manager'),
(2, 'Deputy General Manager, Opertaions'),
(3, 'Deputy General Manager, Resources Managment'),
(4, 'Business Dev.t and Planning'),
(5, 'Underwriting and Branch Support'),
(7, 'Finance'),
(8, 'Human Resource Managment and Prop.Admin'),
(9, 'Life'),
(10, 'Audit and Inspection'),
(11, 'Engineering '),
(12, 'Risk and Compliance Management'),
(13, 'Legal Services'),
(14, 'Information Technology'),
(16, 'Board Office'),
(17, 'Eastern Region Claims'),
(18, 'Western Region Claims');

-- --------------------------------------------------------

--
-- Table structure for table `disiplinaryinfo`
--

CREATE TABLE `disiplinaryinfo` (
  `Id` int(11) NOT NULL,
  `EmpID` int(11) NOT NULL,
  `DisplinaryText` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `DesicionTaken` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `disiplinaryinfo`
--

INSERT INTO `disiplinaryinfo` (`Id`, `EmpID`, `DisplinaryText`, `DesicionTaken`, `date`) VALUES
(8, 1, 'Fighting With Staff', '2 Month Salary Ban', '2018-09-28'),
(9, 2, 'Fighting with staff', 'One Month Salary Penalty', '2018-11-15');

-- --------------------------------------------------------

--
-- Table structure for table `empcertefications`
--

CREATE TABLE `empcertefications` (
  `Id` int(11) NOT NULL,
  `EmpID` int(11) NOT NULL,
  `CerteficationID` int(11) NOT NULL,
  `CerteficationName` varchar(500) NOT NULL,
  `GivenBy` varchar(500) NOT NULL,
  `CertfiedDate` date DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empcertefications`
--

INSERT INTO `empcertefications` (`Id`, `EmpID`, `CerteficationID`, `CerteficationName`, `GivenBy`, `CertfiedDate`) VALUES
(1, 1, 3, 'CISCO I', 'CISCO', '2017-10-11'),
(3, 1, 2, 'CISCO II', 'CISCO', '2018-03-12'),
(39, 2, 6, 'Certified Management Accountant ', 'CMA', '2018-11-15'),
(16, 1, 14, 'Cisco Certified Network Associate (CCNA)', 'CISCO', '2018-09-03'),
(17, 1, 15, 'Cisco Certified Network Professional (CCNP)', 'CISCO', '2018-09-03'),
(19, 1, 12, 'Certified Bank Auditor', 'CBA', '2018-09-21'),
(20, 1, 7, 'Enrolled Agent', 'EA', '2018-09-21'),
(25, 1, 5, 'Certified Financial Analyst', 'CFA', '2018-09-21'),
(28, 1, 5, 'Certified Financial Analyst', 'CFA', '2018-09-21'),
(29, 1, 12, 'Certified Bank Auditor', 'CBA', '2018-09-21'),
(30, 1, 15, 'Cisco Certified Network Professional (CCNP)', 'CISCO', '2018-09-21'),
(33, 21, 5, 'Certified Financial Analyst', 'CFA', '2018-09-21'),
(34, 21, 6, 'Certified Management Accountant ', 'CMA', '2018-09-21'),
(35, 21, 8, 'Certified Internal Auditor', 'CIA', '2018-09-21'),
(36, 21, 12, 'Certified Bank Auditor', 'CBA', '2018-09-21'),
(37, 21, 13, 'Chartered Alternative Investment Analyst', 'CAIA', '2018-09-21'),
(38, 20, 15, 'Cisco Certified Network Professional (CCNP)', 'CISCO', '2018-09-25'),
(40, 2, 9, 'Certified Information Systems Auditor', 'CISA', '2018-11-15');

-- --------------------------------------------------------

--
-- Table structure for table `empleave`
--

CREATE TABLE `empleave` (
  `Id` int(11) NOT NULL,
  `EmpID` int(11) NOT NULL,
  `LeaveTaken` double NOT NULL,
  `LeaveStartDate` date NOT NULL,
  `LeaveEndDate` date NOT NULL,
  `LeaveType` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `PayStatus` varchar(30) NOT NULL,
  `leaveEntA` double NOT NULL,
  `leaveEntB` double NOT NULL,
  `leaveEntryDate` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `empleave`
--

INSERT INTO `empleave` (`Id`, `EmpID`, `LeaveTaken`, `LeaveStartDate`, `LeaveEndDate`, `LeaveType`, `PayStatus`, `leaveEntA`, `leaveEntB`, `leaveEntryDate`) VALUES
(23, 1, 3, '2018-09-04', '2018-09-07', 'Sick Leave', 'Paid', 4, 14.3375, '2018-11-02'),
(24, 1, 2, '2018-09-19', '2018-09-22', 'Sick Leave', 'Paid', 1, 14.3375, '2018-11-02'),
(22, 2, 2, '2018-09-01', '2018-09-03', 'Sick Leave', 'Paid', 0, 9.9069444444444, '2018-11-05'),
(25, 2, 2, '2018-09-20', '2018-09-22', 'Sick Leave', 'Paid', 0, 7.906944444444401, '2018-11-06'),
(26, 2, 2, '2018-09-06', '2018-09-08', 'Annual Leave', 'Paid', 0, 5.906944444444401, '2018-11-06'),
(32, 7, 5, '2018-09-05', '2018-09-15', 'Annual Leave', 'Paid', 10, 18.152777777778, '2018-11-07'),
(29, 2, 2, '2018-09-14', '2018-09-16', 'Exam Leave', 'Paid', 0, 3.9069444444444006, '2018-11-08'),
(30, 2, 2, '2018-09-14', '2018-09-16', 'Sick Leave', 'Paid', 0, 3.9069444444444006, '2018-11-09'),
(51, 53, 4, '2018-11-06', '2018-08-10', 'Exam Leave', 'Paid', 0, 2.8313888888889, '2018-11-15'),
(42, 44, 5, '2018-09-05', '2018-09-10', 'Sick Leave', 'Paid', 12, 18, '2018-11-03'),
(45, 1, 2, '2018-09-07', '2018-09-09', 'Annual Leave', 'Paid', 3, 10.3375, '2018-11-13'),
(46, 2, 10, '2018-09-02', '2018-09-12', 'Weeding Leave', 'UnPaid', 0.9069444444444, 5.3388888888889, '2018-11-12'),
(47, 2, 1, '2018-09-28', '2018-09-29', 'Annual Leave', 'Paid', 0.9069444444444, 7.3527777777778, '2018-11-08'),
(48, 55, 5, '2018-11-01', '2018-11-05', 'Mourning Leave', 'Paid', 12, 26.291666666667, '2018-11-14'),
(49, 51, 5, '2018-11-10', '2018-11-15', 'Mourning Leave', 'Paid', 2, 14.513888888889, '2018-11-14'),
(50, 52, 2, '2018-11-01', '2018-11-03', 'Mourning Leave', 'Paid', 8, 14.222222222222, '2018-11-14'),
(52, 55, 5, '2018-11-20', '2018-11-25', 'Mourning Leave', 'Paid', 12, 26.291666666667, '2018-11-15'),
(53, 53, 10, '2018-11-01', '2018-11-10', 'Weeding Leave', 'Paid', 0, 2.8313888888889, '2018-11-15');

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `Emp_ID` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `grand_name` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `sex` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `nationality` varchar(500) CHARACTER SET utf32 COLLATE utf32_unicode_ci NOT NULL,
  `address_country` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address_city` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `address_SubCity` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `address_HouseNumber` int(11) DEFAULT NULL,
  `address_phone` varchar(15) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `address_email` varchar(500) DEFAULT NULL,
  `date_of_employment` date NOT NULL,
  `educational_level` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `grade` int(11) NOT NULL,
  `salary` double NOT NULL,
  `allowance_Representation` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `allowance_Transport` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `empimg` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `Department` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `Branch` varchar(500) NOT NULL,
  `LeaveEntitilmentA` double NOT NULL,
  `LeaveEntitilmentB` double NOT NULL,
  `ExpiredLeave` double NOT NULL,
  `PerformaceValue` double DEFAULT NULL,
  `Status` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `Emp_ID`, `first_name`, `last_name`, `grand_name`, `sex`, `date_of_birth`, `nationality`, `address_country`, `address_city`, `address_SubCity`, `address_HouseNumber`, `address_phone`, `address_email`, `date_of_employment`, `educational_level`, `position`, `grade`, `salary`, `allowance_Representation`, `allowance_Transport`, `empimg`, `Department`, `Branch`, `LeaveEntitilmentA`, `LeaveEntitilmentB`, `ExpiredLeave`, `PerformaceValue`, `Status`) VALUES
(1, 'NIC3432', 'Hewi', 'Paul', 'Nuri', 'Male', '1994-12-13', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Kolfe', 909, '+2510922306868', 'hewipoul@gmail.com', '2010-01-01', 'Bsc.', 'IT Manager', 14, 22000, '345345', '0', 'images/1534491603170820182.jpg', 'Information Technology', 'Head Office', 10.3375, 11, 13, 5, 0),
(2, 'NIC3433', 'Samuel', 'Samson', 'Assefaw', 'Male', '1991-04-22', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Bole', 987, '+251923434565', 'samuelsamson@gmail.com', '2018-08-01', 'Bsc.', 'Software Developer', 8, 9700, '600', '300', 'images/1534494351170820182.jpg', 'Information Technology', 'Ras', 0, 9.2805555555555, 3, 5, 0),
(6, 'NIC3434', 'Lionel', 'Andress', 'Messi', 'Male', '1986-08-21', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Bole', 989, '+251988234234', 'messi@gmail.com', '2018-08-15', 'Msc.', 'Claim', 9, 10000, '24234', '234234', 'images/1534491615170820182.jpg', 'Eastern Region Claims', 'Head Office', 10.906944444444, 8.6736111111111, 15, 3, 0),
(7, 'NIC3444', 'Kidus', 'Kasaye', 'Tekilu', 'Male', '2018-08-15', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Kolfe', 987, '09875645656', 'dagizg@gmail.com', '2018-01-09', 'Msc.', 'General Manager', 12, 10000, '34234', '23423', 'images/1534750968200820182.jpg', 'Eastern Region Claims', 'Head Office', 18.152777777778, 14.541666666666, 5, 4.5, 1),
(12, 'NIC3435', 'Mulualem', 'Gulmaw', 'Gulye', 'Male', '1988-02-09', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Ayer Tena', 546, '0911657899', 'mulye@gmail.com', '2015-02-18', 'Bsc.', 'Database', 12, 19000, '0', '300', 'images/1535533948290820181.jpg', 'Information Technology', 'Head Office', 12.198611111111, 16.243055555555, 16, 4, 1),
(15, 'NIC3442', 'Samrawit', 'Ayalew', 'Kebede', 'Female', '1987-06-17', 'Ethiopian', 'Ethiopia', 'Adama', 'Zero Zetegn', 566, '09123456789', 'samri@gmail.com', '2012-02-07', 'Bsc.', 'Database Admin', 8, 9000, '0', '300', 'images/user.png', 'Business Dev.t and Planning', 'Adama', 24, 17.993055555556, 0, 4.5, 1),
(16, 'NIC3436', 'Zeleke', 'Gessese', 'Burka', 'Male', '1964-06-17', 'Ethiopian', 'Ethiopia', 'Adama', 'Bole', 445, '0913679834', 'zelewa@gmail.com', '2015-02-17', 'Msc.', 'Branch Manager', 12, 16000, '600', '1200', 'images/user.png', 'Business Dev.t and Planning', 'Adama', 24, 16.243055555555, 0, 2, 1),
(17, 'NIC3437', 'Yimegnu', 'Ayele', 'Bekele', 'Female', '1979-02-06', 'Ethiopian', 'Ethiopia', 'Jimma', 'Aba Jifar', 345, '0922345556', 'yimebeke@yahoo.com', '2014-01-29', 'Bsc.', 'Branch Manager', 11, 15000, '500', '1000', 'images/user.png', 'Underwriting and Branch Support', 'Jimma', 22, 16.875, 2, 4, 1),
(20, 'NIC3438', 'Abreham', 'Teferi', 'Kebede', 'Male', '1987-02-03', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Kolfe', 345, '0922345467', 'abrish@yahoo.com', '2015-02-10', 'MA.', 'Underwriting Head', 12, 10000, '1500', '1000', 'images/user.png', 'Legal Services', 'Head Office', 24, 16.243055555555, 16, 5, 1),
(19, 'NIC3439', 'Muse', 'Zekarias', 'Kebede', 'Male', '1984-02-08', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Kolfe', 455, '0912344356', 'muse@gmail.com', '2017-07-06', 'Msc.', 'Underwriting Head', 12, 12000, '1000', '1500', 'images/user.png', 'Audit and Inspection', 'Head Office', 2, 14.833333333334, 0, 5, 1),
(21, 'NIC3430', 'Amelmal', 'Addis', 'Girma', 'Female', '1989-02-08', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Arada', 454, '0922567678', 'ameliti@gmail.com', '2015-02-11', 'BA.', 'Clerk', 7, 7000, '0', '500', 'images/1537532703210920181.png', 'Western Region Claims', 'Head Office', 14, 11.343055555556, 10, 4, 1),
(44, 'NIC3441', 'Aster', 'Aweke', 'Girma', 'Female', '1985-03-13', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Bole', 564, '0923456578', 'astu@gmail.com', '2015-03-18', 'BA.', 'IT Manager', 8, 7000, '0', '500', 'images/1537865537250920181.png', 'Information Technology', 'Head Office', 18, 16.194444444445, 12, 4.6, 1),
(49, 'NIC5678', 'Hawariyaw', 'Pawulos', ' ', 'Male', '1985-02-02', 'Ethiopian', 'Ethiopia', 'Addis Ababa', ' ', 988, '911324567', 'hewipoul@gmail.com', '2017-04-24', 'Bsc.', 'IT Officer', 6, 5000, '0', '300', 'images/user.png', 'Information Technology', 'Head Office', 0, 18.870833333333, 10, 5, 1),
(50, 'NIC5679', 'Dagim', 'Pawulos', ' ', 'Male', '1991-04-13', 'Ethiopian', 'Ethiopia', 'Addis Ababa', ' ', 456, '922347678', 'dagiz@yahoo.com', '2015-05-25', 'Bsc.', 'Underwriting Officer', 8, 7000, '0', '300', 'images/user.png', 'Western Region Claims', 'Head Office', 12, 28.590277777778, 2, 4, 1),
(51, 'NIC3578', 'Samuel', 'Samson', ' ', 'Male', '1981-03-19', 'Ethiopian', 'Ethiopia', 'Addis Ababa', ' ', 754, '915657687', 'sami@gmail.com', '2012-09-26', 'Msc.', 'Network Adminstrator', 9, 10000, '200', '500', 'images/user.png', 'Information Technology', 'Head Office', 2, 17.034722222222, 8, 4.5, 1),
(52, '', 'Sebele', 'Adamu', 'Birihanu', 'Female', '1981-02-24', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Bole', 66676, '0923434367', 'seblicho@yahoo.com', '2016-03-10', 'MA.', 'Branch Manager', 12, 25000, '1000', '2000', 'images/user.png', 'General Manager', 'Gurd Shola', 8, 16.451388888889, 32, 4.5, 1),
(53, 'NIC77787', 'Ismet', 'Ali', 'Tokchoglu', 'Male', '1986-02-05', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Mekanisa', 5454, '0923434345', 'ismet@yahoo.com', '2016-06-15', 'Msc.', 'IT Manager', 7, 12000, '0', '1500', 'images/user.png', 'Information Technology', 'Head Office', 0, 4.3397222222222, 0, 0, 1),
(54, 'NIC5678', 'Hawariyaw', 'Pawulos', 'Nuri', 'Male', '1985-02-02', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Bole', 988, '911324567', 'hewipoul@gmail.com', '2017-04-24', 'Bsc.', 'IT Officer', 6, 5000, '0', '300', 'images/user.png', 'Information Technology', 'Head Office', 0, 18.870833333333, 10, 5, 1),
(55, 'NIC5679', 'Dagim', 'Pawulos', 'Tesfaye', 'Male', '1991-04-13', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Kolfe', 456, '922347678', 'dagiz@yahoo.com', '2015-05-25', 'Bsc.', 'Underwriting Officer', 8, 7000, '0', '300', 'images/user.png', 'Eastern Region Claims', 'Head Office', 12, 28.590277777778, 2, 4, 1),
(56, 'NIC3578', 'Samuel', 'Samson', ' ', 'Male', '1981-03-19', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Nifas Silk', 754, '915657687', 'sami@gmail.com', '2012-09-26', 'Msc.', 'Network Adminstrator', 9, 10000, '200', '500', 'images/user.png', 'Information Technology', 'Head Office', 2, 17.034722222222, 8, 4.5, 1),
(57, 'NIC0012', 'Zebenay', 'Hailu', 'Merga', 'Female', '1990-02-06', 'Ethiopian', 'Ethiopia', 'Addis Ababa', 'Kolfe', 2343, '0912323454', 'zebi@gmail.com', '2015-02-11', 'BA.', 'Clerk', 6, 4497, '0', '300', 'images/user.png', 'Eastern Region Claims', 'Head Office', 0, 2.9494444444444, 0, 4.5, 1);

-- --------------------------------------------------------

--
-- Table structure for table `employeetransfers`
--

CREATE TABLE `employeetransfers` (
  `Id` int(11) NOT NULL,
  `EmpID` int(11) NOT NULL,
  `position` varchar(500) NOT NULL,
  `grade` int(11) NOT NULL,
  `salary` varchar(500) NOT NULL,
  `department` varchar(500) NOT NULL,
  `Branch` varchar(500) NOT NULL,
  `EndDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employeetransfers`
--

INSERT INTO `employeetransfers` (`Id`, `EmpID`, `position`, `grade`, `salary`, `department`, `Branch`, `EndDate`) VALUES
(4, 1, 'Application Dev.t Officer', 9, '13000', 'Information Technology', 'Head Office', NULL),
(5, 1, 'IT Technician', 8, '8500', 'Information Technology', 'Head Office', NULL),
(7, 1, 'Website Developer', 7, '18500', 'Information Technology', 'Head Office', NULL),
(8, 1, 'System and Data Analyst', 13, '18000', 'Information Technology', 'Head Office', NULL),
(9, 2, 'IT Head', 12, '18000', 'Information Technology', 'Head Office', '1970-01-01'),
(10, 2, 'Website Developer', 8, '12000', 'Information Technology', 'Head Office', '2018-09-06'),
(11, 6, 'General Manager', 7, '50000', 'Finance', 'Head Office', '1970-01-01'),
(12, 12, 'Under Writing', 9, '10000', 'Human Resource Managment and Prop.Admin', 'Head Office', '2018-09-06'),
(13, 2, 'Network Admin', 11, '16000', 'Information Technology', 'Head Office', '2018-11-04');

-- --------------------------------------------------------

--
-- Table structure for table `grade`
--

CREATE TABLE `grade` (
  `Id` int(11) NOT NULL,
  `GradeValue` int(11) NOT NULL,
  `GradeName` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `grade`
--

INSERT INTO `grade` (`Id`, `GradeValue`, `GradeName`) VALUES
(1, 1, 'Grade 1'),
(2, 2, 'Grade 2'),
(3, 3, 'Grade 3'),
(4, 4, 'Grade 4'),
(5, 5, 'Grade 5'),
(6, 6, 'Grade 6'),
(7, 7, 'Grade 7'),
(8, 8, 'Grade 8'),
(9, 9, 'Grade 9'),
(10, 10, 'Grade 10'),
(11, 11, 'Grade 11'),
(12, 12, 'Grade 12'),
(13, 13, 'Grade 13'),
(14, 14, 'Grade 14'),
(16, 15, 'Grade 15');

-- --------------------------------------------------------

--
-- Table structure for table `hrusers`
--

CREATE TABLE `hrusers` (
  `Id` int(11) NOT NULL,
  `username` varchar(500) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `password` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `isAdmin` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hrusers`
--

INSERT INTO `hrusers` (`Id`, `username`, `password`, `isAdmin`) VALUES
(1, 'hewipoul', '7b647ab372fb068607f02d387f48c1c6', 1),
(4, 'tesfa', 'f0f4bae1f0be8ab1e6e86780d16d21da', 0),
(6, 'jonny', '17f1df9f24dcdbbad02ae0f620e4ca53', 0);

-- --------------------------------------------------------

--
-- Table structure for table `leavetype`
--

CREATE TABLE `leavetype` (
  `Id` int(11) NOT NULL,
  `LeaveName` varchar(500) NOT NULL,
  `Deduction` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `leavetype`
--

INSERT INTO `leavetype` (`Id`, `LeaveName`, `Deduction`) VALUES
(4, 'Annual Leave', 1),
(5, 'Sick Leave', 0),
(7, 'Exam Leave', 0),
(8, 'Mourning Leave', 0),
(9, 'Weeding Leave', 0),
(10, 'Other Leaves', 0);

-- --------------------------------------------------------

--
-- Table structure for table `resignations`
--

CREATE TABLE `resignations` (
  `Id` int(11) NOT NULL,
  `EmpID` int(11) NOT NULL,
  `Type` varchar(50) NOT NULL,
  `Reason` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `resignations`
--

INSERT INTO `resignations` (`Id`, `EmpID`, `Type`, `Reason`, `date`) VALUES
(7, 1, '', 'Insufficient Salary', '2018-10-01'),
(19, 2, 'Death', 'Training...', '2018-11-28'),
(20, 6, 'Death', 'Training', '2018-11-28');

-- --------------------------------------------------------

--
-- Table structure for table `yearlookup`
--

CREATE TABLE `yearlookup` (
  `Id` int(11) NOT NULL,
  `CurrentYear` year(4) NOT NULL,
  `CurrentMonth` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `yearlookup`
--

INSERT INTO `yearlookup` (`Id`, `CurrentYear`, `CurrentMonth`) VALUES
(1, 2018, 12);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `certefications`
--
ALTER TABLE `certefications`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `disiplinaryinfo`
--
ALTER TABLE `disiplinaryinfo`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `empcertefications`
--
ALTER TABLE `empcertefications`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `empleave`
--
ALTER TABLE `empleave`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`,`Emp_ID`);

--
-- Indexes for table `employeetransfers`
--
ALTER TABLE `employeetransfers`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `grade`
--
ALTER TABLE `grade`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `hrusers`
--
ALTER TABLE `hrusers`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `leavetype`
--
ALTER TABLE `leavetype`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `resignations`
--
ALTER TABLE `resignations`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `yearlookup`
--
ALTER TABLE `yearlookup`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `certefications`
--
ALTER TABLE `certefications`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `disiplinaryinfo`
--
ALTER TABLE `disiplinaryinfo`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `empcertefications`
--
ALTER TABLE `empcertefications`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
--
-- AUTO_INCREMENT for table `empleave`
--
ALTER TABLE `empleave`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;
--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;
--
-- AUTO_INCREMENT for table `employeetransfers`
--
ALTER TABLE `employeetransfers`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `grade`
--
ALTER TABLE `grade`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `hrusers`
--
ALTER TABLE `hrusers`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `leavetype`
--
ALTER TABLE `leavetype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `resignations`
--
ALTER TABLE `resignations`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `yearlookup`
--
ALTER TABLE `yearlookup`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
