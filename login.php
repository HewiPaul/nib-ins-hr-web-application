<?php session_start(); $session_value = (isset($_SESSION['error']))?$_SESSION['error']:''; ?>
<!DOCTYPE html>
<html lang="en" ng-app="NibInsuranceEManagment">
<head>
	<title>Nib Insurance S.Co.</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="shortcut icon" href="images/NIB Logo.png" type="image/x-icon">
<!--===============================================================================================-->	
<link rel="shortcut icon" href="images/nib.png" type="image/x-icon">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor_/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor_/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="vendor_/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="css/util.css">
	<link rel="stylesheet" type="text/css" href="css/main.css">
<!--===============================================================================================-->
<link rel="stylesheet" href="css/font-awesome.css">
<link rel="stylesheet" href="css/fontawesome-all.css">
</head>
<body>
	<div class="size1 bg0 where1-parent">
		<!-- Coutdown -->
		<div class="flex-c-m bg-img1 size2 where1 overlay1 where2 respon2" style="background-image: url('images/nibmain.jpg');">
			<div class="wsize2 flex-w flex-c-m cd100 js-tilt">
				<div class="row">
						<form class="form-group" action="services/Login.php" method="post" name="login">
								<h1 style="color:rgb(250, 243, 244); margin: 20px">Log In</h1>
								<span style="float: left; padding-top: 5px"><i class="fa fa-user"></i></span><input style="margin: 20px; width: 400px" class="form-control" type="text" name="username" placeholder="Username" required />
								<span style="float: left; padding-top: 5px"><i class="fa fa-key"></i></span><input style="margin: 20px; width: 400px" class="form-control" type="password" name="password" placeholder="Password" required />
								<input style="margin: 20px; width: 100px" class="btn btn-success" name="submit" type="submit" onClick="showAlert()" value="Login" />
						</form>
				</div>
			</div>
		</div>
		
		<!-- Form -->
		<div class="size3 flex-col-sb flex-w p-l-75 p-r-75 p-t-45 p-b-45 respon1">
			<div class="wrap-pic1">
				<img class="img img-responsive" src="images/logo.png" alt="LOGO">
			</div>

			<div class="p-t-50 p-b-60">
				<form class="contact100-form validate-form">
					<img class="img img-responsive" src="images/HR Logo.png"/>
				</form>
				<p class="m1-txt1 p-b-36">
					<h1 style="color:rgb(201, 9, 41); font-weight: bold">Nib Insurance S.Co.</h1><h4 style="color:rgb(97, 47, 1); font-weight: bold">Your Number One Choice!!</h4>
				</p>
			</div>
		</div>
	</div>


    <script src="scripts/sweetAlert.js"></script>
	<script>
	message();
	function message() {
	   var content = document.createElement('div');
       var error = '<?php echo $session_value; ?>';
       if(error == "success"){
        content.innerHTML = 'Login Succesfull, <strong> WelCome! </strong>';
        swal({
            title: "Success",
            content: content,
            icon: "success",
            button: false
        });
       	setTimeout(function(){
          var unsetSession = '<?php unset($_SESSION['error']);?>';
          window.location.href = 'index.php';
         }, 1500);
        }
       else if(error == "failed"){
         content.innerHTML = 'Invalid <strong> Username </strong> or <strong> Password </strong> Used!';
       	 swal({
          title: "Invalid Login!",
          content: content,
          text: "",
          icon: "warning",
          button:false
          });
       setTimeout(function(){
       var unsetSession = '<?php unset($_SESSION['error']);?>';
       window.location.href = 'login.php';
       }, 1500);
       }
	};
</script>

<!--===============================================================================================-->	
	<script src="vendor_/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor_/bootstrap/js/popper.js"></script>
	<script src="vendor_/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor_/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="vendor_/countdowntime/moment.min.js"></script>
	<script src="vendor_/countdowntime/moment-timezone.min.js"></script>
	<script src="vendor_/countdowntime/moment-timezone-with-data.min.js"></script>
	<script src="vendor_/countdowntime/countdowntime.js"></script>
	<script src="scripts/fontawesome.js"></script>
    <script src="scripts/fontawesome-all.js"></script>
	<script>
		$('.cd100').countdown100({
			/*Set Endtime here*/
			/*Endtime must be > current time*/
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: 7,
			endtimeHours: 18,
			endtimeMinutes: 0,
			endtimeSeconds: 0,
			timeZone: "" 
			// ex:  timeZone: "America/New_York"
			//go to " http://momentjs.com/timezone/ " to get timezone
		});
	</script>
<!--===============================================================================================-->
	<script src="vendor_/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
    <script src="scripts/sweetAlert.js"></script>
    <script src="scripts/angular.js"></script>
    <script src="scripts/angular.min.js"></script>
    <script src="scripts/angular.js"></script>
    <script src="scripts/angular-route.js"></script>
    <script src="scripts/dirPagination.js"></script>
    <script src="controllers/PageController.js"></script>  
</body>
</html>