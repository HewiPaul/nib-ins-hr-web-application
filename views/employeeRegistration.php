<?php
include("../services/auth.php");
?>
<link rel="stylesheet" href="dist/css/select2.css">

<div class="col-lg-12" ng-controller="employeeController">
      <ul class="nav nav-tabs">
            <li class="active">
                  <a href=".addhome" data-toggle="tab">New Employee</a>
            </li>
            <?php
               if($_SESSION['isAdmin']){
            ?>
            <li>
                  <a href=".excelparser" data-toggle="tab">Parse From Excel</a>
            </li>
            <?php
              }
            ?>
      </ul>

      <!-- Tab panes -->
      <div class="tab-content">
            <div class="tab-pane fade in active addhome">
                  <div style="padding: 15px;" class="row">
                        <div class="panel panel-default">
                              <div class="panel-heading">
                                    Employee Registration Form.
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body">
                                    <div class="col-md-8">
                                          <form id="frmUploader" enctype="multipart/form-data" style="padding: 5px; padding-top: 10px"
                                                role="form" name="form">
                                                <div class="row">
                                                      <div class="form-group">
                                                            <label class="control-label" for="inputError">Employee
                                                                  Photo</label>
                                                            <input id="image1" type="file" style="max-width:550px"
                                                                  ng-model="image1" accept="image/*" onchange="angular.element(this).scope().uploadedFile(this)"
                                                                  class="form-control">
                                                            <img id="output" style="margin-top: 5px; width: 120px; height: 120px;"
                                                                  class="img img-responsive img-circle" src="images/user.png">
                                                      </div>
                                                      <div class="form-group">
                                                            <div style="padding-bottom:5px;" class="col-md-12">
                                                                  <label class="control-label" for="inputError">EmployeeId</label>
                                                                  <input type="text" class="form-control" ng-model="empId"
                                                                        placeholder="Employee Id" data-ng-required="true">
                                                            </div>
                                                      </div>
                                                      <div style="padding-top:5px" class="form-group">
                                                            <div class="col-lg-4 col-md-6">
                                                                  <label class="control-label" for="inputSuccess">First
                                                                        Name</label>
                                                                  <input type="text" class="form-control" ng-model="fname"
                                                                        placeholder="First name" data-ng-required="true">
                                                            </div>
                                                            <div class="col-lg-4 col-md-6">
                                                                  <label class="control-label" for="inputWarning">Last
                                                                        Name</label>
                                                                  <input type="text" class="form-control" ng-model="lname"
                                                                        placeholder="First name" data-ng-required="true">
                                                            </div>
                                                            <div class="col-lg-4 col-md-6">
                                                                  <label class="control-label" for="inputError">Grand
                                                                        Name</label>
                                                                  <input type="text" class="form-control" ng-model="gname"
                                                                        placeholder="Grand name">
                                                            </div>
                                                      </div>
                                                </div>
                                                <div style="padding-top: 5px" class="form-group">
                                                      <label>Gender</label>
                                                      <select id="sex" class="js-example-basic-single form-control"
                                                            name="state" ng-model="sex" class="form-control">
                                                            <option value="" selected disabled hidden>Choose here</option>
                                                            <option>Male</option>
                                                            <option>Female</option>
                                                      </select>
                                                </div>
                                                <div style="padding-top: 5px" class="form-group">
                                                      <label class="control-label" for="inputError">Birth Date</label>
                                                      <input id="dob" type="date" value="" class="form-control"
                                                            ng-model="dob" max="{{minAge | date:'yyyy-MM-dd'}}" min="{{maxAge | date:'yyyy-MM-dd'}}"
                                                            data-ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Nationality</label>
                                                      <input type="text" class="form-control" ng-model="nationality"
                                                            placeholder="Nationality" ng-init="nationality ='Ethiopian'"
                                                            data-ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Country</label>
                                                      <input type="text" class="form-control" placeholder="Country"
                                                            ng-model="country" ng-init="country ='Ethiopia'"
                                                            data-ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">City</label>
                                                      <input type="text" class="form-control" placeholder="City"
                                                            ng-model="city" data-ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Sub City</label>
                                                      <input type="text" class="form-control" placeholder="Sub City"
                                                            ng-model="subCity">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">House Number</label>
                                                      <input type="text" class="form-control" placeholder="House Number"
                                                            ng-model="houseno">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Phone</label>
                                                      <input type="text" class="form-control" placeholder="Phone"
                                                            ng-model="phone" data-ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Email</label>
                                                      <input type="email" class="form-control" placeholder="Email"
                                                            ng-model="email">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Hire date</label>
                                                      <input id="hiredate" type="date" value="" class="form-control"
                                                            placeholder="Hire date" ng-model="hiredate"
                                                            data-ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Education Level</label>
                                                      <select id="edulevel" class="js-example-basic-single form-control"
                                                            name="state" ng-model="edulevel" class="form-control"
                                                            data-ng-required="true">
                                                            <option value="" selected disabled hidden>Choose here</option>
                                                            <option>Primary School (1-8)</option>
                                                            <option>High School Completed</option>
                                                            <option>12th Completed</option>
                                                            <option>Certificate</option>
                                                            <option>Diploma</option>
                                                            <option>Bsc.</option>
                                                            <option>BA.</option>
                                                            <option>Msc.</option>
                                                            <option>MA.</option>
                                                            <option>Phd.</option>
                                                            <option>Asst Prof.</option>
                                                            <option>Prof.</option>
                                                      </select>
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Position</label>
                                                      <input id="position" type="text" class="form-control" placeholder="Position"
                                                            ng-model="position" data-ng-required="true">
                                                </div>
                                                <div style="padding-top: 5px" class="form-group">
                                                      <label class="control-label" for="inputError">Grade</label>
                                                      <select id="grade" class="js-example-basic-single form-control"
                                                            name="state" class="form-control" ng-model="grade"
                                                            data-ng-change="setInitLeave(grade)" required>
                                                            <option value="" selected disabled hidden>Choose here</option>
                                                            <option value="{{grd.GradeValue}}" ng-repeat="grd in gradesList">{{grd.GradeName}}</option>
                                                      </select>
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Salary</label>
                                                      <input type="number" class="form-control" placeholder="Salary"
                                                            ng-model="salary" step="any" data-ng-required="true">
                                                </div>
                                                <div style="padding-top: 5px" class="form-group">
                                                      <label class="control-label" for="inputError">Branch</label>
                                                      <select id="Branch" class="js-example-basic-single form-control"
                                                            name="state" class="form-control" ng-model="Branch"
                                                            required>
                                                            <option value="" selected disabled hidden>Choose here</option>
                                                            <option value="{{brn.BranchName}}" ng-repeat="brn in branchesList">{{brn.BranchName}}</option>
                                                      </select>
                                                </div>
                                                <div style="padding-top: 5px" class="form-group">
                                                      <label class="control-label" for="inputError">Department</label>
                                                      <select class="js-example-basic-single form-control" name="state"
                                                            id="department" class="form-control" ng-model="department"
                                                            required>
                                                            <option value="" selected disabled hidden>Choose here</option>
                                                            <option value="{{dep.DepartmentName}}" ng-repeat="dep in departmentsList">{{dep.DepartmentName}}</option>
                                                      </select>
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Leave Entitlement
                                                            (A)</label>
                                                      <input id="LeaveEntitlementA" type="number" class="form-control"
                                                            placeholder="Leave Entitlement (A)" ng-model="LeaveEntitlementA"
                                                            ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Leave Entitlement
                                                            (B)</label>
                                                      <input id="LeaveEntitlementB" type="number" class="form-control"
                                                            placeholder="Leave Entitlement (B)" ng-model="LeaveEntitlementB"
                                                            step="any" ng-required="true">
                                                </div>
                                                <div class="form-group">
                                                      <label class="control-label" for="inputError">Expired Leave</label>
                                                      <input id="ExpLeave" type="number" class="form-control"
                                                            placeholder="Expired Leave" ng-model="ExpLeave" ng-required="true">
                                                </div>
                                                <div class="row">
                                                      <div class="form-group">
                                                            <div class="col-lg-4 col-md-6">
                                                                  <label class="control-label" for="inputSuccess">Representation</label>
                                                                  <input type="number" step="any" class="form-control"
                                                                        ng-model="rep" placeholder="Representation"
                                                                        data-ng-required="true">
                                                            </div>
                                                            <div class="col-lg-4 col-md-6">
                                                                  <label class="control-label" for="inputSuccess">Transport</label>
                                                                  <input type="number" step="any" class="form-control"
                                                                        ng-model="trans" placeholder="Transport"
                                                                        data-ng-required="true">
                                                            </div>
                                                            <div class="col-lg-4 col-md-6">
                                                                  <label class="control-label" for="inputError">Performance
                                                                        Value</label>
                                                                  <input id="performace" type="number" step="any" class="form-control"
                                                                        ng-model="performace" placeholder="Performance Value"
                                                                        data-ng-required="true">
                                                            </div>
                                                      </div>
                                                </div>
                                                <input style="margin-top: 20px;padding:7px; width:130px" class="btn btn-success"
                                                      type="submit" value="Submit" ng-click="form.$valid && SaveEmployee()" />
                                          </form>
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
            <?php
               if($_SESSION['isAdmin']){
            ?>
            <div class="tab-pane fade excelparser">
                  <div style="padding: 15px;" class="row">
                        <div class="panel panel-default">
                              <div class="panel-heading">
                                    Parse Employee Data From Excel Sheet
                              </div>
                              <!-- /.panel-heading -->
                              <div class="panel-body">
                                    <div class="form-group">
                                          <br>
                                          <label class="control-label" for="inputError">Excel File (.csv)</label>
                                          <input type="file" id="files" name="files" ng-model="csvFileUpload" class="form-control">
                                          <br>
                                          <!--<input style="padding: 5px;" class="btn btn-success" type="button" value="Parse" onclick="UploadCSV()">-->
                                    </div>
                                    <div style="padding: 15px; overflow-x: scroll" class="row">
                                          <table id="table" class="table table-striped table-bordered table-hover table-responsive">
                                                <thead style="background-color: rgb(225, 225, 255)">
                                                      <th>Employee Id</th>
                                                      <th>Name of Employee</th>
                                                      <th>Sex</th>
                                                      <th>Date Of Birth</th>
                                                      <th>Date of Employment</th>
                                                      <th>Educational Level</th>
                                                      <th>Position</th>
                                                      <th>Grade</th>
                                                      <th>Salary</th>
                                                      <th>Representation</th>
                                                      <th>Transport</th>
                                                      <th>Branch</th>
                                                      <th>Department</th>
                                                      <th>Leave Entitlement A</th>
                                                      <th>Leave Entitlement B</th>
                                                      <th>Expired Leave</th>
                                                      <th>Country</th>
                                                      <th>Nationality</th>
                                                      <th>City</th>
                                                      <th>SubCity</th>
                                                      <th>house_number</th>
                                                      <th>phone</th>
                                                      <th>email</th>
                                                      <th>Performance Value</th>
                                                      <th>Status</th>
                                                </thead>
                                          </table>
                                          <input id="exportBtn" disabled="true" type="submit" class="btn btn-warning"
                                                ng-click="SaveParsedCSVtoDB()" value="Export To Database">
                                    </div>
                              </div>
                        </div>
                  </div>
            </div>
            <?php
              }
            ?>
      </div>
</div>
<script src="scripts/select2.min.js"></script>
<script src="scripts/xlsx.js"></script>
<script>
      // In your Javascript (external .js resource or <script> tag)
      $(document).ready(function () {
            $('.js-example-basic-single').select2();
            $('#files').change(handleFile);
      });
</script>