<?php
include("../services/auth.php");
?>
<link href="css/dataTable.css" rel="stylesheet">
<style>
    .selected {
        background-color: rgb(205, 248, 196);
    }
</style>
<div class="col-lg-12" ng-controller="leaveController">
    <div class="panel panel-default">
        <div class="panel-heading">
             Employees Leave History.
        </div>
        <div class="panel-body">
            <div style="padding:10px; overflow: auto" class="row">
                <table id="myTable" class="ng-cloak table table-bordered table-hover table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead class="bg-warning" style="color: rgb(66, 65, 65)">
                        <tr>
                            <th>#</th>
                             <?php
                                    if($_SESSION['isAdmin']){
                             ?>
                            <th>Actions</th>
                             <?php
                               }
                             ?>
                            <th>Employee Id</th>
                            <th>Employee Name
                            </th>
                            <th>Date of Employment
                            </th>
                            <th>Department
                            </th>
                            <!--<th ng-click="sort('position')">Job Title
                                <span class="glyphicon sort-icon" ng-show="sortKey=='position'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('grade')">Grade
                                <span class="glyphicon sort-icon" ng-show="sortKey=='grade'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('salary')">Salary
                                <span class="glyphicon sort-icon" ng-show="sortKey=='salary'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th>Cut of Date</th>
                            <th>Experience Up to the cut of Date</th>-->
                            <th>Leave Type</th>
                            <th>Leave Entitlement (A)</th>
                            <th>Leave Entitlement (B)</th>
                            <th>Leave Total A+B </th>
                            <th>Leave Taken</th>
                            <th>Pay Status</th>
                            <th>Leave Start Date</th>
                            <th>Leave End Date</th>
                            <th>Remaining Leave</th>
                            <th>Remaining Leave in Monetary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="row in leaves" ng-class="{'selected':$index == selectedRow}"
                            ng-click="setClickedRow($index)" ng-model="search" ng-cloak>
                            <td>{{$index + 1}}</td>
                            <?php
                              if($_SESSION['isAdmin']){
                            ?>
                            <td style="white-space: nowrap">
                                <!--<a class="btn btn-success" type="button" name="edit" ng-click="FetchLeaveDetail(row.Id)"><i class="fa fa-file-alt"></i></a>
                                        <a class="btn btn-warning" type="button" name="edit" ng-click="FetchLeave(row.Id)"><i class="fa fa-edit"></i></a>-->
                                <a class="btn btn-danger" type="button" name="delete" ng-click="DeleteLeave(row.Id, row.EmpID, row.LeaveTaken, row.LeaveType)">
                                    <i class="fa fa-trash-alt"></i>
                                </a>
                            </td>
                             <?php
                                   }
                              ?>
                            <td ng-bind="::row.Emp_ID"></td>
                            <td ng-bind="::row.first_name + ' ' + row.last_name"></td>
                            <td ng-bind="::row.date_of_employment"></td>
                            <td ng-bind="::row.Department"></td>
                            <!--<td>{{row.position}}</td>
                            <td>{{row.grade}}</td>
                            <td>{{row.salary}}</td>
                            <td>{{zare = today}}</td>
                            <td>{{calculateExperieanceWithCutOfDate(row.date_of_employment , today)}}</td>-->
                            <td ng-bind="::row.LeaveType"></td>
                            <td ng-bind="::row.leaveEntA | number : 2"></td>
                            <td ng-bind="::row.leaveEntB | number : 2"></td>
                            <td ng-bind="totalLeave = row.leaveEntA*1 + row.leaveEntB*1 | number : 2"></td>
                            <td ng-bind="::row.LeaveTaken"></td>
                            <td ng-bind="::row.PayStatus"></td>
                            <td ng-bind="::row.LeaveStartDate"></td>
                            <td ng-bind="::row.LeaveEndDate"></td>
                            <td ng-if="row.LeaveType == 'Annual Leave'" ng-bind="totalLeave*1 - row.LeaveTaken*1 | number : 2"></td>
                            <td ng-if="row.LeaveType != 'Annual Leave'" ng-bind="totalLeave*1 | number : 2"></td>
                            <td ng-if="row.LeaveType == 'Annual Leave'" ng-bind="((row.salary*1)/24)*(totalLeave*1 - row.LeaveTaken*1) | number : 2"></td>
                            <td ng-if="row.LeaveType != 'Annual Leave'" ng-bind="((row.salary*1)/24)*(totalLeave*1) | number : 2"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>


        <div class="panel panel-default">
        <div class="panel-heading">
             Employees Leave Info.
        </div>
        <div class="panel-body">
            <div style="padding:10px; overflow: auto" class="row">
                <table id="myTable2" class="ng-cloak table table-bordered table-hover table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
                    <thead style="background-color: rgb(252, 238, 249); color: rgb(66, 65, 65)">
                        <tr>
                            <th>#</th>
                            <th>Employee Id</th>
                            <th>Employee Name
                            </th>
                            <th>Date of Employment
                            </th>
                            <!--<th ng-click="sort('position')">Job Title
                                <span class="glyphicon sort-icon" ng-show="sortKey=='position'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('grade')">Grade
                                <span class="glyphicon sort-icon" ng-show="sortKey=='grade'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th ng-click="sort('salary')">Salary
                                <span class="glyphicon sort-icon" ng-show="sortKey=='salary'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th>Cut of Date</th>-->
                            <th>Branch</th>
                            <th>Department
                                <span class="glyphicon sort-icon" ng-show="sortKey=='Department'" ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                            </th>
                            <th>Grade</th>
                            <th>Salary</th>
                            <th>Experieance</th>
                            <th>Leave Entitlement (A)</th>
                            <th>Leave Entitlement (B)</th>
                            <th>Leave Total A+B </th>
                            <th>Remaining Leave in Monetary</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="row in Allemps" ng-class="{'selected':$index == selectedRow}"
                            ng-click="setClickedRow($index)" ng-model="search" ng-cloak>
                            <td>{{$index + 1}}</td>
                            <td ng-bind="::row.Emp_ID"></td>
                            <td ng-bind="::row.first_name + ' ' + row.last_name"></td>
                            <td ng-bind="::row.date_of_employment"></td>
                            <td ng-bind="::row.Branch"></td>
                            <td ng-bind="::row.Department"></td>
                            <td ng-bind="::row.grade"></td>
                            <td ng-bind="::row.salary"></td>
                            <td ng-bind="calculateExperieanceWithCutOfDate(row.date_of_employment , today)"></td>
                            <td ng-bind="::row.LeaveEntitilmentA | number : 2"></td>
                            <td ng-bind="::row.LeaveEntitilmentB | number : 2"></td>
                            <td ng-bind="totalLeave = row.LeaveEntitilmentA*1 + row.LeaveEntitilmentB*1 | number : 2"></td>
                            <td ng-bind="((row.salary*1)/24)*(totalLeave*1) | number : 2"></td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
    <!-- /.row -->
</div>

<script>

$(function () {
setTimeout(function(){
$('#myTable').DataTable( {
responsive: true
});
$('#myTable2').DataTable( {
responsive: true
});
}, 100);
});

</script>