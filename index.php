<?php
include("services/auth.php");
?>
<!DOCTYPE html>
<html lang="en" ng-app="NibInsuranceEManagment">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nib Insurance Employee Managment</title>
    <link rel="shortcut icon" href="images/NIB Logo.png" type="image/x-icon">
    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <!-- MetisMenu CSS -->
    <link href="vendor/metisMenu/metisMenu.min.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <!-- Morris Charts CSS -->
    <link href="vendor/morrisjs/morris.css" rel="stylesheet">
    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="assets/css/font-awesome.css">
    <link rel="stylesheet" href="assets/css/fontawesome-all.css">
    <link rel="stylesheet" href="dist/css/dataTable.css">
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/select2.min.css">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body ng-controller="mainController">

    <div id="wrapper">
        <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#/"><img style="float:left;" width="30" height="30" class="img img-responsive" src="images/NIB Logo.png"><label style="margin-top: 6px;  color: rgba(247, 128, 148, 0.979)">Nib Insurance S.Co</label></a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                        <li class="divider">
                            <i class="fa fa-user fa-fw"></i> Hi, <?php echo $_SESSION['username']; ?>
                        </li>
                        <li class="divider">
                            <a href="#/changePassword"><i class="fa fa-cog fa-fw"></i> Settings</a>
                        </li>
                        <li class="divider">
                        <a href="services/logout.php"><i class="fa fa-sign-out-alt fa-fw"></i> Logout</a>
                        </li>
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <a href="#/" style="font-size: 18px; text-decoration: none; color: rgb(2, 2, 68)"><i class="menu-icon fa fa-tachometer-alt"></i><span style="padding-left: 20px "> Dashboard</span></a>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li class="menu-item">
                            <a href="#/newemployee" ng-click="refreshPage()"><i class="menu-icon fa fa-user-plus"></i><span style="padding-left: 16px"> Add Employee</span></a>
                        </li>
                        <li class="menu-item">
                            <a href="#/Addleave" ng-click="refreshPage()"><i class="menu-icon fa fa-walking"></i><span style="padding-left: 20px"> Add Leave</span></a>
                        </li>
                        <li class="menu-item">
                            <a href="#/employees"><i class="menu-icon fa fa-users"></i> <span style="padding-left: 17px"> Employees</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <li class="menu-item">
                            <a href="#/leaves"><i class="menu-icon fa fa-sign-out-alt"></i><span style="padding-left: 17px"> Leave History</span></a>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php
                        if($_SESSION['isAdmin']){
                        ?>
                        <li>
                            <a href="#"><i class="menu-icon fa fa-fw fa-table"></i><span style="padding-left: 18px">Manage Attributes</span><i style="float:right" class="fa fa-arrow-down"></i></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#/AddDepartment"><i class="menu-icon fa fa-folder-open"></i><span style="padding-left: 16px"> Manage Department</span></a>
                                </li>
                                <li>
                                    <a href="#/AddBranch" ng-click="refreshPage()"><i class="menu-icon fa fa-code-branch"></i><span style="padding-left: 20px"> Manage Branch</span></a>
                                </li>
                                <li>
                                    <a href="#/AddGrade"><i class="menu-icon fa fa-align-justify"></i><span style="padding-left: 18px"> Manage Grade</span></a>
                                </li>
                                <li>
                                        <a href="#/AddCereteficates"><i class="menu-icon fa fa-certificate"></i><span style="padding-left: 16px"> Manage Certefications</span></a>
                                </li>
                                <li>
                                    <a href="#/AddLeaveType" ng-click="refreshPage()"><i class="menu-icon fa fa fa-walking"></i><span style="padding-left: 16px"> Manage Leave Type</span></a>
                            </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php
                        }
                        ?>


                        <li>
                            <a href="#"><i class="menu-icon fa fa-fw fa-file-alt"></i><span style="padding-left: 18px">Reports</span><i style="float:right" class="fa fa-arrow-down"></i></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="#/LeaveReport"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 16px"> Leave Report</span></a>
                                </li>
                                <li>
                                    <a href="#/GenralLeaveReport"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 16px"> General Leave Report</span></a>
                                </li>
                                <li>
                                    <a href="#/EmployeeReport"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 16px"> Employee Report</span></a>
                                </li>
                                <li>
                                    <a href="#/EmployeeFollowUpReportS"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 16px"> Staff Follow Up (S)</span></a>
                                </li>
                                <li>
                                    <a href="#/EmployeeFollowUpReportSE"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 16px"> Staff Follow Up (S Edu.)</span></a>
                                </li>
                                <li>
                                    <a href="#/EmployeeFollowUpReportSOcc"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 16px"> Staff Follow Up (S Occ.)</span></a>
                                </li>
                                 <li>
                                    <a href="#/SizeofHumanResources"><i class="menu-icon fa fa-file-excel"></i><span style="padding-left: 16px"> Size of HR (LSEA)</span></a>
                                </li>
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 style="margin-left: 10px" class="page-header"><i class="menu-icon fa fa-tachometer-alt"></i> HR Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div style="min-height:65vh; background-color: white" class="row">
                <ng-view>
        
                </ng-view>
            </div>
            <div style="bottom: 0px; text-align: center">
                <hr>
                <label><span style="font-weight:300;"> © </span> 2018 - Designed and Developed By Hawariyaw Pawulos</label>
            </div>
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="scripts/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="vendor/metisMenu/metisMenu.min.js"></script>
    <!-- Morris Charts JavaScript -->
    <script src="vendor/raphael/raphael.min.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="dist/js/sb-admin-2.js"></script>
    <script src="scripts/fontawesome.js"></script>
    <script src="scripts/fontawesome-all.js"></script>
     <!-- DataTables JavaScript -->
     <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
     <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
     <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>
    <script src="scripts/sweetAlert.js"></script>
    <script src="scripts/quick-ng-repate.js"></script>
    <script src="scripts/select2.min.js"></script> 
    <script src="scripts/angular.js"></script>
    <script src="scripts/angular.min.js"></script>
    <script src="scripts/angular.js"></script>
    <script src="scripts/angular-route.js"></script>
    <script src="scripts/dirPagination.js"></script>
    <script src="controllers/PageController.js"></script>  
</body>
</html>