<?php
require_once 'db.php';
$data = json_decode(file_get_contents("php://input"));
$count_ExecutivesM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND (`grade` = '14' OR `grade` = '15') AND `Status` = '1'") or die(mysqli_error());
$count_TopManagementM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND (`grade` = '12' OR `grade` = '13') AND `Status` = '1'") or die(mysqli_error());
$count_ManagerialTechnicalM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND `grade` = '11' AND `Status` = '1'") or die(mysqli_error());
$count_MiddleManagmentM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND `grade` = '10' AND `Status` = '1'") or die(mysqli_error());
$count_ProfessionalSupervisoryM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND (`grade` = '9' OR `grade` = '8') AND `Status` = '1'") or die(mysqli_error());
$count_ProfessionalM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND `grade` = '7' AND `Status` = '1'") or die(mysqli_error());
$count_SemiProfessionalsM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND `grade` = '6' AND `Status` = '1'") or die(mysqli_error());
$count_ClericalM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND (`grade` = '3' OR `grade` = '4') AND `Status` = '1'") or die(mysqli_error());
$count_AuxiliaryM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND `grade` = '2' AND `Status` = '1'") or die(mysqli_error());
$count_CustodialM = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Male' AND `grade` = '1' AND `Status` = '1'") or die(mysqli_error());

$count_ExecutivesF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND (`grade` = '14' OR `grade` = '15') AND `Status` = '1'") or die(mysqli_error());
$count_TopManagementF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND (`grade` = '12' OR `grade` = '13') AND `Status` = '1'") or die(mysqli_error());
$count_ManagerialTechnicalF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND `grade` = '11' AND `Status` = '1'") or die(mysqli_error());
$count_MiddleManagmentF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND `grade` = '10' AND `Status` = '1'") or die(mysqli_error());
$count_ProfessionalSupervisoryF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND (`grade` = '9' OR `grade` = '8') AND `Status` = '1'") or die(mysqli_error());
$count_ProfessionalF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND `grade` = '7' AND `Status` = '1'") or die(mysqli_error());
$count_SemiProfessionalsF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND `grade` = '6' AND `Status` = '1'") or die(mysqli_error());
$count_ClericalF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND (`grade` = '3' OR `grade` = '4') AND `Status` = '1'") or die(mysqli_error());
$count_AuxiliaryF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND `grade` = '2' AND `Status` = '1'") or die(mysqli_error());
$count_CustodialF = $conn->query("SELECT Count(id) FROM employees WHERE `sex` = 'Female' AND `grade` = '1' AND `Status` = '1'") or die(mysqli_error());

$data1 = $count_ExecutivesM->fetch_array();
$data2 = $count_TopManagementM->fetch_array();
$data3 = $count_ManagerialTechnicalM->fetch_array();
$data4 = $count_MiddleManagmentM->fetch_array();
$data5 = $count_ProfessionalSupervisoryM->fetch_array();
$data6 = $count_ProfessionalM->fetch_array();
$data7 = $count_SemiProfessionalsM->fetch_array();
$data8 = $count_ClericalM->fetch_array();
$data9 = $count_AuxiliaryM->fetch_array();
$data10 = $count_CustodialM->fetch_array();

$data11 = $count_ExecutivesF->fetch_array();
$data22 = $count_TopManagementF->fetch_array();
$data33 = $count_ManagerialTechnicalF->fetch_array();
$data44 = $count_MiddleManagmentF->fetch_array();
$data55 = $count_ProfessionalSupervisoryF->fetch_array();
$data66 = $count_ProfessionalF->fetch_array();
$data77 = $count_SemiProfessionalsF->fetch_array();
$data88 = $count_ClericalF->fetch_array();
$data99 = $count_AuxiliaryF->fetch_array();
$data00 = $count_CustodialF->fetch_array();

$data = array();
$data = [$data1[0], $data2[0], $data3[0], $data4[0], $data5[0], $data6[0], $data7[0], $data8[0], $data9[0], $data10[0], $data11[0], $data22[0], $data33[0], $data44[0], $data55[0], $data66[0], $data77[0], $data88[0], $data99[0], $data00[0]];
echo json_encode($data);
?>