<?php
include("../services/auth.php");
?>
<link href="css/dataTable.css" rel="stylesheet">

<div width="100%" class="col-lg-12" ng-controller="employeeController">
    <div class="panel panel-default">
        <div class="panel-heading">
            Employees
        </div>
        <!-- /.panel-heading -->
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active">
                    <a href=".employee" data-toggle="tab">Employees</a>
                </li>
                <li>
                    <a href=".employeenotfication" data-toggle="tab">Employee Notifications</a>
                </li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div style="padding:10px;" class="tab-pane fade in active employee">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Employees List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div style="padding-right:15px" class="row">
                                <form class="form-inline">
                                    <div style="padding:15px" class="input-group">
                                        Display All Employees in DB: <input type="checkbox" id="empCheck" ng-model="checkbox"
                                            ng-change="fetchAllEmps()">
                                    </div>
                                </form>
                            </div>
                            <div style="padding:15px;overflow:auto" class="row">
                                <table id="myTable" class="ng-cloak table table-bordered table-hover table-responsive dt-responsive nowrap"
                                    cellspacing="0" width="100%">
                                    <thead style="background-color: rgb(239, 238, 252); color: rgb(66, 65, 65)">
                                        <tr>
                                            <th>#</th>
                                            <th>Actions</th>
                                            <th>Employee ID
                                            </th>
                                            <th>Full Name
                                            </th>
                                            <th>Sex                                              
                                            </th>
                                            <!--<td>Age</td>-->
                                            <th>Education
                                            </th>
                                            <th>Position
                                            </th>
                                            <th>Department
                                            </th>
                                            <th>Grade                                            
                                            </th>
                                            <th>Salary
                                            </th>
                                            <th>Experieance
                                            </th>
                                            <th>Status
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr ng-repeat="row in employees|orderBy:sortKey:reverse|filter:search"
                                            ng-model="search">
                                            <td>{{$index + 1}}</td>
                                            <td style="white-space: nowrap">
                                                <a class="btn btn-success" type="button" name="view" href="#/viewEmployee"
                                                    ng-click="FetchEmployeeDetail(row.id)"><i class="fa fa-file-alt"></i></a>
                                                <?php
                                                    if($_SESSION['isAdmin']){
                                                ?>
                                                <a class="btn btn-warning" type="button" name="edit" href="#/editEmployee"
                                                    ng-click="FetchEmployee(row.id)"><i class="fa fa-edit"></i></a>
                                                <a style="pointer-events: none; cursor: default;" class="btn btn-danger"
                                                    type="button" name="delete" ng-click="DeleteEmployee(row.id)"><i
                                                        class="fa fa-trash-alt"></i></a>
                                                <?php
                                                    }
                                                ?>
                                            </td>
                                            <td ng-bind="::row.Emp_ID"></td>
                                            <td ng-bind="::row.first_name + ' ' + row.last_name"></td>
                                            <td ng-bind="::row.sex"></td>
                                            <!--<td>{{calculateAge(row.date_of_birth)}}</td>-->
                                            <td ng-bind="::row.educational_level"></td>
                                            <td ng-bind="::row.position"></td>
                                            <td ng-bind="::row.Department"></td>
                                            <td ng-bind="Grade + ' ' + row.grade"></td>
                                            <td ng-bind="::row.salary"></td>
                                            <td ng-bind="calculateExperieance(row.date_of_employment)"></td>
                                            <td ng-if="row.Status == '1'">Active</td>
                                            <td ng-if="row.Status == '0'">Not Active</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="tab-pane fade employeenotfication">
                    <div class="row">
                        <div style="padding:10px" class="col-md-12">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    Employee Close To Retire
                                </div>
                                <!-- /.panel-heading -->
                                <div class="panel-body">
                                    <div style="padding: 15px" class="row">
                                        <form class="form-inline">
                                            <div style="float:right; padding-top: 15px; padding-bottom: 15px" class="input-group">
                                                <input class="form-control" type="text" ng-model="search2" placeholder="Search"
                                                    type="search" />
                                                <span class="input-group-addon">
                                                    <span class="fa fa-search"></span>
                                                </span>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="row" style="overflow-x:scroll; padding: 15px">
                                    <table id="myTable" class="ng-cloak table table-bordered table-hover table-responsive dt-responsive nowrap" cellspacing="0" width="100%">
                                            <thead style="background-color: rgb(239, 238, 252); color: rgb(66, 65, 65)">
                                                <tr>
                                                    <td>Actions</td>
                                                    <td>Profile</td>
                                                    <td ng-click="sort('Emp_ID')">Employee ID
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='Emp_ID'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td ng-click="sort('first_name')">Full Name
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='first_name'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td ng-click="sort('sex')">Sex
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='sex'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td>Age</td>
                                                    <td ng-click="sort('educational_level')">Education
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='educational_level'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td ng-click="sort('position')">Position
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='position'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td ng-click="sort('Department')">Department
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='Department'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td ng-click="sort('grade')">Grade
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='grade'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td ng-click="sort('salary')">Salary
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='salary'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td ng-click="sort('date_of_employment')">Experieance
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='date_of_employment'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                    <td>Age</td>
                                                    <td ng-click="sort('Status')">Status
                                                        <span class="glyphicon sort-icon" ng-show="sortKey=='Status'"
                                                            ng-class="{'glyphicon-chevron-up':reverse,'glyphicon-chevron-down':!reverse}"></span>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody ng-init="fetchEmployeesAboutToRetire()">
                                                <tr ng-repeat="row in employeesToRetire|orderBy:sortKey:reverse|filter:search2"
                                                    ng-model="search2">
                                                    <td style="white-space: nowrap">
                                                        <a class="btn btn-success" type="button" name="view" href="#/viewEmployee"
                                                            ng-click="FetchEmployeeDetail(row.id)"><i class="fa fa-file-alt"></i></a>
                                                             <?php
                                                                if($_SESSION['isAdmin']){
                                                             ?>
                                                        <a class="btn btn-warning" type="button" name="edit" href="#/editEmployee"
                                                            ng-click="FetchEmployee(row.id)"><i class="fa fa-edit"></i></a>
                                                        <a style="pointer-events: none; cursor: default;" class="btn btn-danger"
                                                            type="button" name="delete" ng-click="DeleteEmployee(row.id)"><i
                                                                class="fa fa-trash-alt"></i></a>
                                                                 <?php
                                                                    }
                                                                 ?>
                                                    </td>
                                                    <td><img style="width:50px; height:50px;" class="img img-responsive img-circle"
                                                            src="{{row.empimg}}"></td>
                                                    <td>{{row.Emp_ID}}</td>
                                                    <td>{{row.first_name + ' ' + row.last_name}}</td>
                                                    <td>{{row.sex}}</td>
                                                    <td>{{calculateAge(row.date_of_birth)}}</td>
                                                    <td>{{row.educational_level}}</td>
                                                    <td>{{row.position}}</td>
                                                    <td>{{row.Department}}</td>
                                                    <td>Grade {{row.grade}}</td>
                                                    <td>{{row.salary}}</td>
                                                    <td>{{calculateExperieance(row.date_of_employment)}}</td>
                                                    <td>{{calculateAge(row.date_of_birth)}}</td>
                                                    <td ng-if="row.Status == '1'">Active</td>
                                                    <td ng-if="row.Status == '0'">Not Active</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
</div>

<script>
$(function () {
    setTimeout(function(){
    $('#myTable').DataTable({
    responsive: true
    });
    }, 100);
});
</script>