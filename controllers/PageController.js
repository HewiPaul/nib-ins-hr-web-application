/// <reference path="../scripts/angular.js" />

var app = angular.module("NibInsuranceEManagment", ["ngRoute", "angularUtils.directives.dirPagination"])
    .config(function ($routeProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "views/home.html",
                controller: "mainController"
            })
            .when("/employees", {
                templateUrl: "views/employees.php",
                controller: "employeeController"
            })
            .when("/newemployee", {
                templateUrl: "views/employeeRegistration.php",
                controller: "employeeController"
            })
            .when("/editEmployee", {
                templateUrl: "views/employeeEdit.html",
                controller: "employeeEditController"
            })
            .when("/viewEmployee", {
                templateUrl: "views/employeeview.html",
                controller: "employeeController"
            })
            .when("/leaves", {
                templateUrl: "views/leaveHistory.php",
                controller: "leaveController"
            })
            .when("/Addleave", {
                templateUrl: "views/leaveRegistration.html",
                controller: "leaveController"
            })
            .when("/AddDepartment", {
                templateUrl: "views/addDepartment.html",
                controller: "DepartemntController"
            })
            .when("/AddBranch", {
                templateUrl: "views/addBranch.html",
                controller: "BranchsController"
            })
            .when("/AddGrade", {
                templateUrl: "views/addGrade.html",
                controller: "GradesController"
            })
            .when("/AddCereteficates", {
                templateUrl: "views/addCereteficates.html",
                controller: "CerteficateController"
            })
            .when("/AddLeaveType", {
                templateUrl: "views/addLeaveType.html",
                controller: "LeaveTypeController"
            })
            .when("/LeaveReport", {
                templateUrl: "views/leaveReport.html",
                controller: "leaveReportController"
            })
            .when("/GenralLeaveReport", {
                templateUrl: "views/GeneralleaveReport.html",
                controller: "leaveReportController"
            })
            .when("/EmployeeReport", {
                templateUrl: "views/employeeReport.html",
                controller: "EmployeeReportController"
            })
            .when("/changePassword", {
                templateUrl: "views/changePassword.php",
                controller: "userController"
            })
            .when("/EmployeeFollowUpReportSE", {
                templateUrl: "views/employeeFollowUpReport.html",
                controller: "EmployeeReportController"
            })
            .when("/EmployeeFollowUpReportS", {
                templateUrl: "views/employeeFollowUpReportS.html",
                controller: "EmployeeReportController"
            })
            .when("/EmployeeFollowUpReportSOcc", {
                templateUrl: "views/EmployeeFollowUpBasedOnSexAndOcc.html",
                controller: "EmployeeReportController"
            })
             .when("/SizeofHumanResources", {
                templateUrl: "views/SizeofHumanResources.html",
                controller: "EmployeeReportController"
            })
           
    });

var curDate = new Date();
var curDay = new Date().getDate();
var curDateMonth = new Date().getMonth() + 1; // we must have to add 1 to get the correcr month
var curDateYear = new Date().getFullYear();
var now = curDateYear + '-' + curDateMonth + '-' + curDay;

app.run(['$http', function ($http) {

    runEveryHour();

    var delay = 60 * 60 * 1000;
    function runEveryHour() {
    console.log("Checking Month Year...");
    $http.post('services/FetchData.php', {
            tableName: 'employees'
        })
        .then(function (res) {
            var employees = res.data;
            $http.post('services/fetchYears.php').then(function (res) {
                var curYear = res.data[0].CurrentYear;
                var curMonth = res.data[0].CurrentMonth;
                if (curYear < curDateYear) {
                    var employee;
                    for (employee of employees) {
                        if (employee.Status == 1) {
                            var employeeHiredDate = new Date(employee.date_of_employment);
                            var YearWorkedInMonth = Noofmonths(employeeHiredDate, curDate);
                            var LaveEntitileA;
                            var LaveEntitileB;
                            var ExpiredLeave;
                            ExpiredLeave = employee.ExpiredLeave * 1 + employee.LeaveEntitilmentA * 1;
                            LaveEntitileA = employee.LeaveEntitilmentB * 1;
                            if (employee.grade >= 8) {
                                LaveEntitileB = (2) + ((YearWorkedInMonth * 1) / 12) / 12;
                            } else {
                                LaveEntitileB = (1.3) + ((YearWorkedInMonth * 1) / 12) / 12;
                            }
                            $http.post('services/adjustLeavePerYear.php', {
                                    id: employee.id,
                                    LeaveEntitilmentA: LaveEntitileA,
                                    LeaveEntitilmentB: LaveEntitileB,
                                    ExpLeave: ExpiredLeave
                                })
                                .success(function (data) {
                                    console.log(data);
                                });
                        };
                    }
                    $http.post('services/changeYearPerYear.php', {
                        CY: curDateYear
                    });
                    $http.post('services/changeMonthPerMonth.php', {
                        CM: curDateMonth
                    });
                };

                if (curMonth < curDateMonth) {
                    var employee;
                    for (employee of employees) {
                        if (employee.Status == 1) {
                            var employeeHiredDate = new Date(employee.date_of_employment);
                            var YearWorkedInMonth = Noofmonths(employeeHiredDate, curDate);
                            //alert(employee.first_name + ' ' + YearWorkedInMonth);
                            var LaveEntitileB;
                            if (employee.grade >= 8) {
                                LaveEntitileB = (employee.LeaveEntitilmentB * 1) + (2) + ((YearWorkedInMonth * 1) / 12) / 12;
                            } else {
                                LaveEntitileB = (employee.LeaveEntitilmentB * 1) + (1.3) + ((YearWorkedInMonth * 1) / 12) / 12;
                            }
                            $http.post('services/adjustEmployeeLeavePerMonth.php', {
                                    id: employee.id,
                                    LeaveEntitilmentB: LaveEntitileB
                                })
                                .success(function (data) {
                                    console.log(data);
                                });
                        };
                    }
                    $http.post('services/changeMonthPerMonth.php', {
                        CM: curDateMonth
                    });
                };
            });
        });
     };

    setInterval(function(){
        runEveryHour();
    },delay);

}]);


app.controller('mainController', function ($scope, $http, $window) {
    $scope.files = [];
    //    Our GET request function
    $scope.calculateAge = function calculateAge(birthday) {
        var ageText = yearCalculator(birthday);
        return ageText;
    };

    $scope.calculateExperieance = function calculateExperieance(dateHired) {
        var ageText = yearCalculator(dateHired);
        if (ageText === "") {
            ageText = "0 years and 0 months";
        }
        return ageText;
    };

    $scope.uploadedFile = function (element) {
        $scope.currentFile = element.files[0];
        var reader = new FileReader();

        reader.onload = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(element.files[0]);

            $scope.image_source = event.target.result
            $scope.$apply(function ($scope) {
                $scope.files = element.files;
            });
        }
        reader.readAsDataURL(element.files[0]);
    };

    $scope.refreshPage = function () {
        window.location.reload();
    };
});



app.controller('userController', function ($scope, $http, $window) {

        $http.post('services/FetchData.php', {tableName: 'hrusers'})
        .then(function (res) {
           $scope.hrusers = res.data;
        }); 

        function fetchusers() {
        $http.post('services/FetchData.php', {tableName: 'hrusers'})
        .then(function (res) {
           $scope.hrusers = res.data;
        }); 
        };

        $scope.resetPwd = function (Id) {
            if (confirm('Are you sure to reset this user password ?')) {
            $http.post('services/resetPassword.php', {
                tableName: 'hrusers',
                id: Id
            }).then(function (response) {
                console.log(response.data);
                alert("User Password Reseted Successfully!");
            });
        }
        };

        $scope.deleteUser = function (Id) {
            swal({
                title: "Are you sure?",
                text: "Once deleted, you will not be able to recover this user!",
                icon: "images/delete.png",
                buttons: true,
                dangerMode: true,
              })
              .then((willDelete) => {
                if (willDelete) {
                  $http.post('services/delete.php', {
                    tableName: 'hrusers',
                    id: Id
                          }).then(function (response) {
                            fetchusers();
                            console.log(response.data);
                            swal("Success", "User Deleted Successfully!" , "success");
                          });
                };
              });
        };

    $scope.changePassword = function () {
        $http({
            method: 'POST',
            url: "services/changePassword.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("uname", document.getElementById("uname").value);
                formData.append("newpass", document.getElementById("newpass").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            document.getElementById("oldpass").value = "";
            document.getElementById("newpass").value = "";
            document.getElementById("Cnewpass").value = "";
            swal("Success", data , "success");
            $window.location.href = '#/';
        });
    };

 $scope.createUser = function () {
        $http({
            method: 'POST',
            url: "services/createUser.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("username", document.getElementById("username").value);
                formData.append("newpasss", document.getElementById("newpasss").value);
                formData.append("isAdmin", document.getElementById("admin").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            fetchusers();
            document.getElementById("username").value = "";
            document.getElementById("newpasss").value = "";
            document.getElementById("Cnewpasss").value = "";
            swal("Success", data , "success");
        });
 };
});


app.controller('employeeController', function ($scope, $http, $window, EmployeesService, DepatmentsService, BranchsService, GradeService) {

    var today = new Date();
    var minAge = 18;
    var maxAge = 60;
    $scope.minAge = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());
    $scope.maxAge = new Date(today.getFullYear() - maxAge, today.getMonth(), today.getDate());

    EmployeesService.saveData().then(function (res) {
        $scope.employees = res.data;
    });

    GradeService.saveData().then(function (res) {
        $scope.gradesList = res.data;
    });

    DepatmentsService.saveData().then(function (res) {
        $scope.departmentsList = res.data;
    });

    BranchsService.saveData().then(function (res) {
        $scope.branchesList = res.data;
    });


    function fetchEmployees() {
        EmployeesService.saveData().then(function (res) {
            $scope.employees = res.data;
        });
    };

    $scope.fetchAllEmps = function () {
        var checkBox = document.getElementById("empCheck");
        // If the checkbox is checked, display the output text
        if (checkBox.checked == true) {
            $http.post('services/fetchdataASC.php', {
                tableName: 'employees'
            }).then(function (response) {
                $scope.employees = response.data;
            });
        } else {
            fetchEmployees();
        }
    };

    $scope.fetchEmployeesAboutToRetire = function () {
        EmployeesService.saveData().then(function (res) {
            var lenght = res.data.length;
            var emps = [];
            var currYear = new Date().getFullYear();
            for (let i = 0; i < lenght; i++) {
                var bornYear = new Date(res.data[i].date_of_birth).getFullYear();
                console.log(res.data[i].first_name + ' ' + (currYear * 1 - bornYear * 1));

                if ((currYear * 1 - bornYear * 1) > 50) {
                    emps.push(res.data[i]);
                }
            }
            $scope.employeesToRetire = emps;
        });
    };

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    $scope.setInitLeave = function (grade) {
        if (grade * 1 < 8) {
            document.getElementById("LeaveEntitlementA").value = 0;
            $scope.LeaveEntitlementA = 0;
            document.getElementById("LeaveEntitlementB").value = 1.33;
            $scope.LeaveEntitlementB = 1.33;
        } else {
            document.getElementById("LeaveEntitlementA").value = 0;
            $scope.LeaveEntitlementA = 0;
            document.getElementById("LeaveEntitlementB").value = 2;
            $scope.LeaveEntitlementB = 2;
        }
    };

    $scope.SaveEmployee = function () {
        try {
            if (document.getElementById("image1").value != "") {
                $scope.image1 = $scope.files[0];
                $http({
                    method: 'POST',
                    url: "services/addEmployee.php",
                    processData: false,
                    transformRequest: function (data) {
                        var formData = new FormData();
                        formData.append("empId", $scope.empId);
                        formData.append("image1", $scope.image1);
                        formData.append("fname", $scope.fname);
                        formData.append("lname", $scope.lname);
                        formData.append("gname", $scope.gname);
                        formData.append("sex", document.getElementById("sex").value);
                        formData.append("dob", document.getElementById("dob").value);
                        formData.append("nationality", $scope.nationality);
                        formData.append("country", $scope.country);
                        formData.append("city", $scope.city);
                        formData.append("subCity", $scope.subCity);
                        formData.append("houseno", $scope.houseno);
                        formData.append("phone", $scope.phone);
                        formData.append("email", $scope.email);
                        formData.append("hiredate", document.getElementById("hiredate").value);
                        formData.append("edulevel", $scope.edulevel);
                        formData.append("position", document.getElementById("position").value);
                        formData.append("grade", document.getElementById("grade").value);
                        formData.append("salary", $scope.salary);
                        formData.append("department", document.getElementById("department").value);
                        formData.append("Branch", document.getElementById("Branch").value);
                        formData.append("LeaveEntitlementA", document.getElementById("LeaveEntitlementA").value);
                        formData.append("LeaveEntitlementB", document.getElementById("LeaveEntitlementB").value);
                        formData.append("ExpLeave", document.getElementById("ExpLeave").value);
                        formData.append("rep", $scope.rep);
                        formData.append("trans", $scope.trans);
                        formData.append("performace", document.getElementById("performace").value);
                        return formData;
                    },
                    data: $scope.form,
                    headers: {
                        'Content-Type': undefined
                    }
                }).success(function (data) {
                    alert(data);
                    $window.location.href = '#/employees';
                    $window.location.reload();
                });
            } else {
                $http({
                    method: 'POST',
                    url: "services/addEmployeewithoutImage.php",
                    processData: false,
                    transformRequest: function (data) {
                        var formData = new FormData();
                        formData.append("empId", $scope.empId);
                        formData.append("fname", $scope.fname);
                        formData.append("lname", $scope.lname);
                        formData.append("gname", $scope.gname);
                        formData.append("sex", document.getElementById("sex").value);
                        formData.append("dob", document.getElementById("dob").value);
                        formData.append("nationality", $scope.nationality);
                        formData.append("country", $scope.country);
                        formData.append("city", $scope.city);
                        formData.append("subCity", $scope.subCity);
                        formData.append("houseno", $scope.houseno);
                        formData.append("phone", $scope.phone);
                        formData.append("email", $scope.email);
                        formData.append("hiredate", document.getElementById("hiredate").value);
                        formData.append("edulevel", $scope.edulevel);
                        formData.append("position", document.getElementById("position").value);
                        formData.append("grade", document.getElementById("grade").value);
                        formData.append("salary", $scope.salary);
                        formData.append("department", document.getElementById("department").value);
                        formData.append("Branch", document.getElementById("Branch").value);
                        formData.append("LeaveEntitlementA", document.getElementById("LeaveEntitlementA").value);
                        formData.append("LeaveEntitlementB", document.getElementById("LeaveEntitlementB").value);
                        formData.append("ExpLeave", document.getElementById("ExpLeave").value);
                        formData.append("rep", $scope.rep);
                        formData.append("trans", $scope.trans);
                        formData.append("performace", document.getElementById("performace").value);
                        return formData;
                    },
                    data: $scope.form,
                    headers: {
                        'Content-Type': undefined
                    }
                }).success(function (data) {
                    swal("Success", data , "success");
                    swal({
                        title: "Success",
                        text: "Employees Created Successfully!",
                        icon: "success"
                      })
                      .then((willOk) => {
                        $window.location.reload();
                        $window.location.href = '#/employees';
                      });
                });
            }
        } catch (error) {
            swal({
                title: "Notice",
                text: "Error Occured while adding new employee." + error,
                icon: "warning",
                dangerMode: true,
              });
        }
    };


    $scope.SaveParsedCSVtoDB = function () {
        if (sessionStorage.parsedRows != null) {
            var employees = sessionStorage.parsedRows;
            console.log(JSON.parse(employees));

            $http({
                method: 'POST',
                url: "services/saveParcedCSV.php",
                processData: false,
                data: JSON.parse(employees),
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                console.log(data);
                swal({
                    title: "Success",
                    text: "Employees Added To Database Successfully!",
                    icon: "success"
                  })
                  .then((willOk) => {
                    $window.location.reload();
                    $window.location.href = '#/employees';
                  });
            });

        }
    };

    
    $scope.printDiv = function (divId) {
        //Get the HTML of div
        var divElements = document.getElementById(divId).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        window.close();
        window.location.reload();
    };

    $scope.FetchEmployeeDetail = function (Id) {
        sessionStorage.SingleEmpId = Id;
        $window.location.href = '#/viewEmployee';
        $window.location.reload();
    };

    $scope.FetchEmployeeCertefication = function () {
        var Id = sessionStorage.SingleEmpId;
        if (Id != null) {
            $http.post('services/fetchcertefication.php', {
                tableName: 'empcertefications',
                id: Id
            }).then(function (response) {
                $scope.Empcertefications = response.data;
            });
        }
    };

    $scope.FetchEmployeeTransfer = function(){
        var Id = sessionStorage.SingleEmpId;
        if (Id != null) {
            $http.post('services/fetchTransfers.php', {
                tableName: 'employeetransfers',
                id: Id
            }).then(function (response) {
                $scope.Emptransfers = response.data;
            });
        }
    };

    $scope.fetchEmployeesDisplinaryCase = function() {
        var Id = sessionStorage.SingleEmpId;
        $http.post('services/fetchEmployeesDisp.php', {
            tableName: 'disiplinaryinfo',
            id: Id
        }).then(function (response) {
            $scope.disciplinaryCases = response.data;
        });
    };

    $scope.fetchEmployeeLeave = function() {
        var Id = sessionStorage.SingleEmpId;
        $http.post('services/fetchEmpLeave.php', {
            tableName: 'empleave',
            id: Id
        }).then(function (response) {
            $scope.empLeave = response.data;
        });
    };

    $scope.fetchEmployeeTotalLeave = function() {
        var Id = sessionStorage.SingleEmpId;
        $http.post('services/fetchsingledata.php', {
            tableName: 'employees',
            id: Id
        }).then(function (response) {
            $scope.LA = response.data[0].LeaveEntitilmentA;
            $scope.LB = response.data[0].LeaveEntitilmentB;
            $scope.Total = (response.data[0].LeaveEntitilmentA*1) + (response.data[0].LeaveEntitilmentB*1);
            $scope.ExpLV = response.data[0].ExpiredLeave
        });
    };

    $scope.setSingleEmployeeValues = function () {
        var Id = sessionStorage.SingleEmpId;
        if (Id != null) {
            $http.post('services/fetchsingledata.php', {
                tableName: 'employees',
                id: Id
            }).then(function (response) {
                console.log(response.data);
                document.getElementById("Id").innerHTML = response.data[0].id;
                document.getElementById("EmpViewHeader").innerHTML = 'Employee Profile - ' + response.data[0].first_name + ' ' + response.data[0].last_name;
                document.getElementById("output2").src = response.data[0].empimg;
                document.getElementById("empID").innerHTML = response.data[0].Emp_ID;
                document.getElementById("fname").innerHTML = response.data[0].first_name;
                document.getElementById("lname").innerHTML = response.data[0].last_name;
                document.getElementById("gname").innerHTML = response.data[0].grand_name;
                document.getElementById("sex").innerHTML = String(response.data[0].sex);
                document.getElementById("dob").innerHTML = response.data[0].date_of_birth;
                document.getElementById("age").innerHTML = yearCalculator(response.data[0].date_of_birth);
                document.getElementById("nationality").innerHTML = response.data[0].nationality;
                document.getElementById("country").innerHTML = response.data[0].address_country;
                document.getElementById("city").innerHTML = response.data[0].address_city;
                document.getElementById("subCity").innerHTML = response.data[0].address_SubCity;
                document.getElementById("houseno").innerHTML = response.data[0].address_HouseNumber;
                document.getElementById("phone").innerHTML = response.data[0].address_phone;
                document.getElementById("email").innerHTML = response.data[0].address_email;
                document.getElementById("hiredate").innerHTML = response.data[0].date_of_employment;
                document.getElementById("edulevel").innerHTML = response.data[0].educational_level;
                document.getElementById("exp").innerHTML = yearCalculator(response.data[0].date_of_employment);
                document.getElementById("position").innerHTML = response.data[0].position;
                document.getElementById("grade").innerHTML = String(response.data[0].grade);
                document.getElementById("salary").innerHTML = response.data[0].salary;
                document.getElementById("department").innerHTML = String(response.data[0].Department);
                document.getElementById("Branch").innerHTML = String(response.data[0].Branch);
                //document.getElementById("LeaveEntitlementA").innerHTML = response.data[0].LeaveEntitilmentA;
                // document.getElementById("LeaveEntitlementB").innerHTML = response.data[0].LeaveEntitilmentB;
                // document.getElementById("ExpLeave").innerHTML = response.data[0].ExpiredLeave;
                // document.getElementById("RL").innerHTML = ((response.data[0].salary * 1) / 24) * ((response.data[0].LeaveEntitilmentA * 1) + (response.data[0].LeaveEntitilmentB * 1));
                document.getElementById("rep").innerHTML = response.data[0].allowance_Representation;
                document.getElementById("trans").innerHTML = response.data[0].allowance_Transport;
                document.getElementById("performace").innerHTML = response.data[0].PerformaceValue;
                if (response.data[0].Status == 1) {
                    document.getElementById("Status").innerHTML = "Active";
                } else {
                    document.getElementById("Status").innerHTML = "Not Active";
                }

            });
        }
    };

    $scope.FetchEmployee = function (Id) { //For Edit 
        sessionStorage.EmpId = Id;
        $window.location.href = '#/editEmployee';
        $window.location.reload();
    };


    $scope.FetchEmployeeFromDetail = function () {
        sessionStorage.EmpId = document.getElementById("Id").innerHTML;
        $window.location.href = '#/editEmployee';
        $window.location.reload();
    };

    $scope.DeleteEmployee = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this employee!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/delete.php', {
                tableName: 'employees',
                id: Id
                      }).then(function (response) {
                        console.log(response.data);
                        fetchEmployees();
                        swal("Success", "Employee Removed Successfully!" , "success");
                      });
            };
          });
    };

    $scope.DeleteEmployeeFromDetail = function () {
        var Id = document.getElementById("Id").innerHTML;
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this employee!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
              $http.post('services/delete.php', {
                tableName: 'employees',
                id: Id
                      }).then(function (response) {
                        console.log(response.data);
                        fetchEmployees();
                        swal("Success", "Employee Removed Successfully!" , "success");
                        $window.location.href = '#/employees';
                      });
            };
          });
    };

    $scope.calculateAge = function yearCalculator(dateAge) {
        var bthDate, curDate, ageYears, ageMonths, ageWeeks, ageDays, ageText;
        bthDate = new Date(dateAge); // Change the birth date here!
        curDate = new Date();
        if (bthDate > curDate) return;
        ageYears = curDate.getFullYear() - bthDate.getFullYear();
        ageMonths = curDate.getMonth() - bthDate.getMonth();
        ageDays = curDate.getDate() - bthDate.getDate();
        if (ageDays < 0) {
            ageMonths = ageMonths - 1;
            ageDays = ageDays + 31;
        }
        ageWeeks = Math.ceil(ageDays / 7);
        if (ageMonths < 0) {
            ageYears = ageYears - 1;
            ageMonths = ageMonths + 12;
        }
        ageText = "";
        if (ageYears > 0) {
            ageText = ageText + ageYears + " year";
            if (ageYears > 1) ageText = ageText + "s";
        }
        if (ageMonths > 0) {
            if (ageYears > 0) {
                if (ageWeeks > 0) ageText = ageText + ", ";
                else if (ageWeeks == 0) ageText = ageText + " and ";
            }
            ageText = ageText + ageMonths + " month";
            if (ageMonths > 1) ageText = ageText + "s";
        }
        if (ageWeeks > 0) {
            if ((ageYears > 0) || (ageMonths > 0)) ageText = ageText + " and ";
            ageText = ageText + ageWeeks + " week";
            if (ageWeeks > 1) ageText = ageText + "s";
        }
        return ageText;
    };

    $scope.refreshPage = function () {
        window.location.reload();
    };

});




app.controller('employeeEditController', function ($scope, $http, $window, DepatmentsService, BranchsService, GradeService, CerteficationsService) {
    $scope.files = [];

    GradeService.saveData().then(function (res) {
        $scope.gradesList = res.data;
    });

    DepatmentsService.saveData().then(function (res) {
        $scope.departmentsList = res.data;
    });

    BranchsService.saveData().then(function (res) {
        $scope.branchesList = res.data;
    });

    CerteficationsService.saveData().then(function (res) {
        $scope.certeficatsList = res.data;
    });

    $scope.fetchCertfs = function () {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchEmployeescert.php', {
            tableName: 'empcertefications',
            id: empId
        }).then(function (response) {
            $scope.empCerteficates = response.data;
        });
    };

    function fetchEmpCerts() {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchEmployeescert.php', {
            tableName: 'empcertefications',
            id: empId
        }).then(function (response) {
            $scope.empCerteficates = response.data;
        });
    };

    $scope.fetchDisciplinaryCases = function () {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchEmployeesDisp.php', {
            tableName: 'disiplinaryinfo',
            id: empId
        }).then(function (response) {
            $scope.disciplinaryCases = response.data;
        });
    };

    function fetchDisCase() {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchEmployeesDisp.php', {
            tableName: 'disiplinaryinfo',
            id: empId
        }).then(function (response) {
            $scope.disciplinaryCases = response.data;
        });
    };

    $scope.fetchTransfers = function () {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchTransfers.php', {
            tableName: 'employeetransfers',
            id: empId
        }).then(function (response) {
            $scope.Employeetransfers = response.data;
        });
    };

    function fetchEmployeesTransfers() {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchTransfers.php', {
            tableName: 'employeetransfers',
            id: empId
        }).then(function (response) {
            $scope.Employeetransfers = response.data;
        });
    };

    $scope.fetchEmployeesDisplinaryCase = function() {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchEmployeesDisp.php', {
            tableName: 'disiplinaryinfo',
            id: empId
        }).then(function (response) {
            $scope.disciplinaryCases = response.data;
        });
    };

    $scope.fetchResigns = function () {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchResigns.php', {
            tableName: 'resignations',
            id: empId
        }).then(function (response) {
            $scope.Employeeresign = response.data;
            if (response.data.length > 0) {
                document.getElementById("reason").disabled = true;
                document.getElementById("resignButton").disabled = true;
            } else {
                document.getElementById("reason").disabled = false;
                document.getElementById("resignButton").disabled = false;
            }
        });
    };

    function fetchResign() {
        var empId = document.getElementById("Id").value;
        $http.post('services/fetchResigns.php', {
            tableName: 'resignations',
            id: empId
        }).then(function (response) {
            $scope.Employeeresign = response.data;
            if (response.data.length > 0) {
                document.getElementById("reason").disabled = true;
                document.getElementById("resignButton").disabled = true;
            } else {
                document.getElementById("reason").disabled = false;
                document.getElementById("resignButton").disabled = false;
            }
        });
    };

    $scope.setValues = function () {
        var Id = sessionStorage.EmpId;
        if (Id != null) {
            $http.post('services/fetchsingledata.php', {
                tableName: 'employees',
                id: Id
            }).then(function (response) {
                console.log(response.data);
                document.getElementById("Id").value = response.data[0].id;
                document.getElementById("empID").value = response.data[0].Emp_ID;
                document.getElementById("output2").src = response.data[0].empimg;
                document.getElementById("fname").value = response.data[0].first_name;
                document.getElementById("lname").value = response.data[0].last_name;
                document.getElementById("gname").value = response.data[0].grand_name;
                document.getElementById("sex").value = String(response.data[0].sex);
                document.getElementById("dob").value = response.data[0].date_of_birth;
                document.getElementById("nationality").value = response.data[0].nationality;
                document.getElementById("country").value = response.data[0].address_country;
                document.getElementById("city").value = response.data[0].address_city;
                document.getElementById("subCity").value = response.data[0].address_SubCity;
                document.getElementById("houseno").value = response.data[0].address_HouseNumber;
                document.getElementById("phone").value = response.data[0].address_phone;
                document.getElementById("email").value = response.data[0].address_email;
                document.getElementById("hiredate").value = response.data[0].date_of_employment;
                document.getElementById("edulevel").value = response.data[0].educational_level;
                document.getElementById("position").value = response.data[0].position;
                document.getElementById("curposition").value = response.data[0].position;
                document.getElementById("grade").value = response.data[0].grade;
                document.getElementById("curgrade").value = response.data[0].grade;
                document.getElementById("salary").value = response.data[0].salary;
                document.getElementById("cursalary").value = response.data[0].salary;
                document.getElementById("department").value = String(response.data[0].Department);
                document.getElementById("curdepartment").value = String(response.data[0].Department);
                document.getElementById("Branch").value = String(response.data[0].Branch);
                document.getElementById("curBranch").value = String(response.data[0].Branch);
                document.getElementById("LeaveEntitlementA").value = response.data[0].LeaveEntitilmentA;
                document.getElementById("LeaveEntitlementB").value = response.data[0].LeaveEntitilmentB;
                document.getElementById("ExpLeave").value = response.data[0].ExpiredLeave;
                document.getElementById("rep").value = response.data[0].allowance_Representation;
                document.getElementById("trans").value = response.data[0].allowance_Transport;
                document.getElementById("performace").value = response.data[0].PerformaceValue;

                document.getElementById("_empID").value = response.data[0].id;
                document.getElementById("EMPID").value = response.data[0].id;
                document.getElementById('today').valueAsDate = new Date();
                document.getElementById('_today').valueAsDate = new Date();
            });
        }
    };


    $scope.UpdateEmployee = function () {
        try {
            $http({
                method: 'POST',
                url: "services/editEmployee.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("id", document.getElementById("Id").value);
                    formData.append("empID", document.getElementById("empID").value);
                    formData.append("fname", document.getElementById("fname").value);
                    formData.append("lname", document.getElementById("lname").value);
                    formData.append("gname", document.getElementById("gname").value);
                    formData.append("sex", document.getElementById("sex").value);
                    formData.append("dob", document.getElementById("dob").value);
                    formData.append("nationality", document.getElementById("nationality").value);
                    formData.append("country", document.getElementById("country").value);
                    formData.append("city", document.getElementById("city").value);
                    formData.append("subCity", document.getElementById("subCity").value);
                    formData.append("houseno", document.getElementById("houseno").value);
                    formData.append("phone", document.getElementById("phone").value);
                    formData.append("email", document.getElementById("email").value);
                    formData.append("hiredate", document.getElementById("hiredate").value);
                    formData.append("edulevel", document.getElementById("edulevel").value);
                    formData.append("position", document.getElementById("position").value);
                    formData.append("grade", document.getElementById("grade").value);
                    formData.append("salary", document.getElementById("salary").value);
                    formData.append("department", document.getElementById("department").value);
                    formData.append("Branch", document.getElementById("Branch").value);
                    formData.append("LeaveEntitlementA", document.getElementById("LeaveEntitlementA").value);
                    formData.append("LeaveEntitlementB", document.getElementById("LeaveEntitlementB").value);
                    formData.append("ExpLeave", document.getElementById("ExpLeave").value);
                    formData.append("rep", document.getElementById("rep").value);
                    formData.append("trans", document.getElementById("trans").value);
                    formData.append("performace", document.getElementById("performace").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                swal({
                    title: "Success",
                    text: data,
                    icon: "success"
                  })
                  .then((willVote) => {
                    $window.location.href = '#/employees';
                    $window.location.reload();
              });
            });
        } catch (error) {
            swal({
                title: "Notice",
                text: "Error occured while updating employee data: " + error,
                icon: "warning",
                dangerMode: true,
            });
        }
    };


    $scope.TransferEmployee = function () {
        try {
            $http({
                method: 'POST',
                url: "services/transferEmployee.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("id", document.getElementById("Id").value);
                    formData.append("position", document.getElementById("newposition").value);
                    formData.append("grade", document.getElementById("newgrade").value);
                    formData.append("salary", document.getElementById("newsalary").value);
                    formData.append("department", document.getElementById("newdepartment").value);
                    formData.append("Branch", document.getElementById("newBranch").value);

                    formData.append("_position", document.getElementById("curposition").value);
                    formData.append("_grade", document.getElementById("curgrade").value);
                    formData.append("_salary", document.getElementById("cursalary").value);
                    formData.append("_department", document.getElementById("curdepartment").value);
                    formData.append("_Branch", document.getElementById("curBranch").value);
                    formData.append("EndDate", now);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                swal({
                    title: "Success",
                    text: data,
                    icon: "success"
                  })
                  .then((willVote) => {
                    fetchEmployeesTransfers();
                    $window.location.reload();
              });
            });
        } catch (error) {
            swal({
                title: "Notice",
                text: "Error occured while updating employee data: " + error,
                icon: "warning",
                dangerMode: true,
            });
        }
    };

    $scope.Resign = function () {
        try {
            $http({
                method: 'POST',
                url: "services/resign.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("id", document.getElementById("Id").value);
                    var type = document.getElementById("ResignType").value;
                    if(type == ""){
                        type = "Resignation";
                    }
                    formData.append("ResignType", type);
                    formData.append("reason", document.getElementById("reason").value);
                    formData.append("_today", document.getElementById("_today").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                swal({
                    title: "Success",
                    text: data,
                    icon: "success"
                  })
                  .then((willVote) => {
                    document.getElementById("reason").value = "";
                    fetchResign();
                });
            });
        } catch (error) {
            swal({
                title: "Notice",
                text: "Error occured while updating employee data: " + error,
                icon: "warning",
                dangerMode: true,
            });
        }
    };


    $scope.UpdateEmployeeImage = function () {
        try {
            $scope.image2 = $scope.files2[0];
            $http({
                method: 'POST',
                url: "services/editEmployeeImage.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("id", document.getElementById("Id").value);
                    formData.append("image2", $scope.image2);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                swal("Success", data , "success");
            });
        } catch (error) {
            swal({
                title:"Notice",
                text: "Please Check That You Choose an Employee Image." + error,
                icon: "warning",
                dangerMode: true,
            });
        }
    };


    $scope.uploadedFile2 = function (element) {
        $scope.currentFile2 = element.files[0];
        var reader = new FileReader();

        reader.onload = function (event) {
            var output = document.getElementById('output2');
            output2.src = URL.createObjectURL(element.files[0]);

            $scope.image_source = event.target.result
            $scope.$apply(function ($scope) {
                $scope.files2 = element.files;
            });
        }
        reader.readAsDataURL(element.files[0]);
    };


    $scope.AddCertficatesToEmployee = function () {
        try {
            var selectCerteficates = String($("#certSelector").val());
            var array = selectCerteficates.split(',');
            array.forEach(function (entry) {
                $http.post('services/fetchsingledata.php', {
                    tableName: 'certefications',
                    id: entry
                }).then(function (response) {
                    console.log(response.data);
                    var crName = response.data[0].CerteficationName;
                    var gBy = response.data[0].GivenBy;
                    $http({
                        method: 'POST',
                        url: "services/addCerteficateForEmployee.php",
                        processData: false,
                        transformRequest: function (data) {
                            var formData = new FormData();
                            formData.append("EmpId", document.getElementById("Id").value);
                            formData.append("CertID", entry);
                            formData.append("crName", crName);
                            formData.append("gBy", gBy);
                            formData.append("dateIssued", document.getElementById("dateIssued").value);
                            return formData;
                        },
                        data: $scope.form,
                        headers: {
                            'Content-Type': undefined
                        }
                    }).success(function (data) {
                        console.log(data);
                    });
                });
            });
            $('#certSelector').val(null).trigger('change');
            swal("Success", "Certeficate Added Successfuly!" , "success");
            fetchEmpCerts();
        } catch (error) {
            swal({
                title:"Notice",
                text: "Error occured during data entry: " + error,
                icon: "warning",
                dangerMode: true,
            });
        }
    };


    $scope.AddDispCaseToEmployee = function () {
        try {
            $http({
                method: 'POST',
                url: "services/addDisciplinaryCaseForEmployee.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("EmpId", document.getElementById("_empID").value);
                    formData.append("disciplinaryTxt", document.getElementById("disciplinaryTxt").value);
                    formData.append("disciplinaryActions", document.getElementById("disciplinaryActions").value);
                    formData.append("today", document.getElementById("today").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                document.getElementById("disciplinaryTxt").value = "";
                document.getElementById("disciplinaryActions").value = "";
                fetchDisCase();
                swal("Success", data , "success");
            });
        } catch (error) {
            swal({
                title:"Notice",
                text: "Error occured during data entry: " + error,
                icon: "warning",
                dangerMode: true,
            });
        }
    };

    $scope.deleteEmpCert = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $http.post('services/delete.php', {
                    tableName: 'empcertefications',
                    id: Id
                      }).then(function (response) {
                        console.log(response.data);
                        fetchEmpCerts();
                        swal("Success", "Certeficate Removed From Employee Successfully!" , "success");
                      });
            };
          });
    };

    $scope.deleteDisp = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $http.post('services/delete.php', {
                    tableName: 'disiplinaryinfo',
                    id: Id
                      }).then(function (response) {
                        console.log(response.data);
                        fetchDisCase();
                        swal("Success", "Disciplinary Case Removed From Employee Successfully!" , "success");
            });
            };
          });
    };

    $scope.deleteTransfers = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $http.post('services/delete.php', {
                    tableName: 'employeetransfers',
                    id: Id
                      }).then(function (response) {
                        console.log(response.data);
                        fetchEmployeesTransfers();
                        swal("Success", "Employee Transfer Removed From Employee Successfully!" , "success");
                      });
            };
          });
    };

    $scope.deleteResign = function (DataId, empId) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                $http.post('services/deleteResignation.php', {
                    tableName: 'resignations',
                    id: DataId,
                    EmpID: empId
                      }).then(function (response) {
                        console.log(response.data);
                        fetchResign();
                        document.getElementById("reason").value = "";
                        swal("Success", "Employee Resignation Info Removed From Employee Successfully!" , "success");
                      });
            };
          });
    };

});

app.controller('leaveController', function ($scope, $http, $window, LeaveService, EmployeesService, LeaveTypeService) {
    $scope.files = [];
    $scope.selectedRow = null; // initialize our variable to null
    
    $scope.setClickedRow = function (index) { //function that sets the value of selectedRow to current index
        $scope.selectedRow = index;
    }

    LeaveService.saveData().then(function (res) {
        $scope.leaves = res.data;
    });

    EmployeesService.saveData().then(function (res) {
        $scope.Allemps = res.data;
    });

    LeaveTypeService.saveData().then(function (res) {
        $scope.leaveTypes = res.data;
    });

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse =! $scope.reverse; //if true make it false and vice versa
    };

    $scope.EmployeeSelected = function (employee) {
        var empID = employee.id;
        $http.post('services/fetchsingledata.php', {
            tableName: 'employees',
            id: empID
        }).then(function (response) {
            document.getElementById('EmpId').value = response.data[0].id;
            document.getElementById('LEA').value = response.data[0].LeaveEntitilmentA;
            document.getElementById('LEB').value = response.data[0].LeaveEntitilmentB;
            console.log(response.data);
        });
    };


    if (sessionStorage.selectedDate != null) {
        $scope.today = sessionStorage.selectedDate;
    } else {
        $scope.today = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
    }

    $scope.calculateExperieanceWithCutOfDate = function calculateExperieanceWithCutOfDate(dateHired, cutOfDate) {
        var ageText = yearCalculatorUptoCutofdate(dateHired, cutOfDate);
        if (ageText === "") {
            ageText = "0 years and 0 months";
        }
        return ageText;
    }

    $scope.SaveLeave = function () {
        try {
            $http({
                method: 'POST',
                url: "services/addLeave.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("EmpId", document.getElementById("EmpId").value);
                    formData.append("leaveStartDate", document.getElementById("leaveStartDate").value);
                    formData.append("leaveEndDate", document.getElementById("leaveEndDate").value);
                    formData.append("leaveTaken", document.getElementById("leaveTaken").value);
                    formData.append("leaveType", document.getElementById("leaveType").value);
                    formData.append("payStat", document.getElementById("payStat").value);
                    formData.append("leaveEntA", document.getElementById("LEA").value);
                    formData.append("leaveEntB", document.getElementById("LEB").value);
                    formData.append("today", document.getElementById("today").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
               console.log("Leave Saved Successfully!");
            });
            var leaveType = document.getElementById("leaveType").value;
            leaveType = leaveType.toLowerCase();

            if (leaveType === "annual leave" || leaveType === "annual") {
                var leavetaken = document.getElementById("leaveTaken").value;
                var empID = document.getElementById("EmpId").value;
                var LeaveEntitilmentA = document.getElementById("LEA").value;
                var LeaveEntitilmentB = document.getElementById("LEB").value;
                LeaveEntitilmentA = (LeaveEntitilmentA * 1) - (leavetaken * 1);
                if (LeaveEntitilmentA * 1 < 0) {
                    LeaveEntitilmentB = (LeaveEntitilmentA * 1) + (LeaveEntitilmentB * 1);
                    LeaveEntitilmentA = 0;
                }
                $http({
                    method: 'POST',
                    url: "services/adjustEmployeeLeave.php",
                    processData: false,
                    transformRequest: function (data) {
                        var formData = new FormData();
                        formData.append("EmpId", empID);
                        formData.append("LEA", LeaveEntitilmentA);
                        formData.append("LEB", LeaveEntitilmentB);
                        return formData;
                    },
                    data: $scope.form,
                    headers: {
                        'Content-Type': undefined
                    }
                }).success(function (data) {
                    console.log(data);
                    swal({
                        title: "Success",
                        text: "Leave Registered Successfully!",
                        icon: "success"
                      })
                      .then((willVote) => {
                        $window.location.href = '#/leaves';
                        $window.location.reload();
                    });
                });
            } else {
                swal({
                    title: "Success",
                    text: "Leave Registered Successfully!",
                    icon: "success"
                  })
                  .then((willVote) => {
                    $window.location.href = '#/leaves';
                    $window.location.reload();
                });
            }
        } catch (error) {
            swal({
                title:"Notice",
                text: 'Failed To Create a Leave: ' + error,
                icon: "warning",
                dangerMode: true,
              });
        }
    }

    $scope.DeleteLeave = function (Id, EmpID, LeaveTaken, LeaveType) {
        if (confirm('Are you sure to delete this leave record ?')) {
            var ltype = LeaveType.toLowerCase();
            if (ltype === 'annual leave' | ltype === 'annual') {
                $http.post('services/deleteLeave.php', {
                    tableName: 'empleave',
                    id: Id,
                    empID: EmpID,
                    leaveTaken: LeaveTaken
                }).then(function (response) {
                    console.log(response.data);
                    swal("Success", "Leave Recored Removed Successfully!", "success");
                    $window.location.reload();
                });
            } else {
                $http.post('services/deleteLeave.php', {
                    tableName: 'empleave',
                    id: Id,
                    empID: EmpID,
                    leaveTaken: 0
                }).then(function (response) {
                    console.log(response.data);
                    swal("Success", "Leave Recored Removed Successfully!", "success");
                    $window.location.reload();
                });
            }
        }
    };

    $scope.refreshPage = function () {
        window.location.reload();
    };
});


app.controller('DepartemntController', function ($scope, $http, $window, DepatmentsService) {

    DepatmentsService.saveData().then(function (res) {
        $scope.departments = res.data;
    });

    function fetchDeps() {
        DepatmentsService.saveData().then(function (res) {
            $scope.departments = res.data;
        });
    };

    $scope.SaveDepartmet = function () {
        $http({
            method: 'POST',
            url: "services/addDepartment.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("depName", document.getElementById("depName").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            fetchDeps();
            document.getElementById("depName").value = "";
            swal("Success", data, "success");
        });
    };

    $scope.deleteDep = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
                $http.post('services/delete.php', {
                    tableName: 'departments',
                    id: Id
              }).then(function (response) {
                console.log(response.data);
                fetchDeps();
                swal("Success", "Department Removed Successfully!", "success");
              });
          });
    };

});

app.controller('BranchsController', function ($scope, $http, $window, BranchsService) {

    BranchsService.saveData().then(function (res) {
        $scope.branches = res.data;
    });

    function fetchBrs() {
        BranchsService.saveData().then(function (res) {
            $scope.branches = res.data;
        });
    };

    $scope.SaveBranch = function () {
        if (document.getElementById("brType").value != null)
            $http({
                method: 'POST',
                url: "services/addBranch.php",
                processData: false,
                transformRequest: function (data) {
                    var formData = new FormData();
                    formData.append("brName", document.getElementById("brName").value);
                    formData.append("brType", document.getElementById("brType").value);
                    return formData;
                },
                data: $scope.form,
                headers: {
                    'Content-Type': undefined
                }
            }).success(function (data) {
                swal({
                    title: "Success",
                    text: data,
                    icon: "success"
                  })
                  .then((willVote) => {
                    $window.location.reload();
                  });
            });
    };

    $scope.deleteBr = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            $http.post('services/delete.php', {
                tableName: 'branches',
                id: Id
              }).then(function (response) {
                console.log(response.data);
                fetchBrs();
                swal("Success", "Branch Removed Successfully!", "success");
              });
          });
    };
});



app.controller('GradesController', function ($scope, $http, $window, GradeService) {
    GradeService.saveData().then(function (res) {
        $scope.grades = res.data;
    });

    function fetchGrs() {
        GradeService.saveData().then(function (res) {
            $scope.grades = res.data;
        });
    };

    $scope.SaveGrade = function () {
        $http({
            method: 'POST',
            url: "services/addGrade.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("grName", document.getElementById("grName").value);
                formData.append("grValue", document.getElementById("grValue").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            fetchGrs();
            document.getElementById("grName").value = "";
            document.getElementById("grValue").value = "";
            swal("Success", data , "success");
        });
    };

    $scope.deleteGr = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            $http.post('services/delete.php', {
                tableName: 'grade',
                id: Id
              }).then(function (response) {
                console.log(response.data);
                fetchGrs();
                swal("Success", "Grade Removed Successfully!", "success");
              });
          });
    };
});


app.controller('CerteficateController', function ($scope, $http, $window, CerteficationsService) {
    CerteficationsService.saveData().then(function (res) {
        $scope.certeficats = res.data;
    });

    function fetchCrs() {
        CerteficationsService.saveData().then(function (res) {
            $scope.certeficats = res.data;
        });
    };

    $scope.sort = function (keyname) {
        $scope.sortKey = keyname; //set the sortKey to the param passed
        $scope.reverse = !$scope.reverse; //if true make it false and vice versa
    }

    $scope.SaveCerteficate = function () {
        $http({
            method: 'POST',
            url: "services/addCerteficate.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("crName", document.getElementById("crName").value);
                formData.append("gBy", document.getElementById("gBy").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            fetchCrs();
            document.getElementById("crName").value = "";
            document.getElementById("gBy").value = "";
            swal("Success", data , "success");
        });
    };

    $scope.deleteCr = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            $http.post('services/delete.php', {
                tableName: 'certefications',
                id: Id
              }).then(function (response) {
                console.log(response.data);
                fetchCrs();
                swal("Success", "Certeficate Removed Successfully!", "success");
              });
          });
    };
});

app.controller('LeaveTypeController', function ($scope, $http, $window, LeaveTypeService) {

    LeaveTypeService.saveData().then(function (res) {
        $scope.leaveTypes = res.data;
    });

    function fetchLType() {
        LeaveTypeService.saveData().then(function (res) {
            $scope.leaveTypes = res.data;
        });
    };

    $scope.SaveLeaveType = function () {
        $http({
            method: 'POST',
            url: "services/addLeaveType.php",
            processData: false,
            transformRequest: function (data) {
                var formData = new FormData();
                formData.append("LName", document.getElementById("LName").value);
                formData.append("Ded", document.getElementById("Ded").value);
                return formData;
            },
            data: $scope.form,
            headers: {
                'Content-Type': undefined
            }
        }).success(function (data) {
            swal({
                title: "Success",
                text: data,
                icon: "success"
              })
              .then((willOk) => {
                $window.location.reload();
              });
        });
    };

    $scope.deleteLType = function (Id) {
        swal({
            title: "Are you sure?",
            text: "Once deleted, you will not be able to recover this data!",
            icon: "images/delete.png",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            $http.post('services/delete.php', {
                tableName: 'leavetype',
                id: Id
              }).then(function (response) {
                console.log(response.data);
                fetchLType();
                swal("Success","Leave Type Removed Successfully!", "success");
              });
          });
    };
});


app.controller('leaveReportController', function ($scope, $http, $window, LeaveService, EmployeesService, DepatmentsService, AABranchsServiceWithOutHO, OutlyingBranchsService) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    $scope.todayDate = dd + '/' + mm + '/' + yyyy;

    LeaveService.saveData().then(function(res){
       $scope.LeavesList = res.data;
    });

    EmployeesService.saveData().then(function (res) {
        $scope.employeesList = res.data;
    });

    DepatmentsService.saveData().then(function (res) {
        $scope.deps = res.data;
    });

    AABranchsServiceWithOutHO.saveData().then(function (res) {
        $scope.AAbranches = res.data;
    });

    OutlyingBranchsService.saveData().then(function (res) {
        $scope.Outlyingbranches = res.data;
    });

    $scope.getTotalLeaveDaysByDepartment = function (department) {
        var total = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            var dep = $scope.employeesList[i].Department;
            var branch = $scope.employeesList[i].Branch.toLowerCase();
            if (dep === department && branch === 'head office') {
                total += ($scope.employeesList[i].LeaveEntitilmentA * 1 + $scope.employeesList[i].LeaveEntitilmentB * 1);
            }
        }
        return total;
    };

    $scope.getTotalLeaveInMonetaryByDepartment = function (department) {
        var totalLeaveInMonitory = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            var dep = $scope.employeesList[i].Department;
            var EmpSalary = $scope.employeesList[i].salary;
            var branch = $scope.employeesList[i].Branch.toLowerCase();
            if (dep === department && branch === 'head office') {
                var totalLeave = ($scope.employeesList[i].LeaveEntitilmentA * 1 + $scope.employeesList[i].LeaveEntitilmentB * 1);
                totalLeaveInMonitory += ((EmpSalary * 1) / 24) * (totalLeave * 1);
            }
        }
        return totalLeaveInMonitory;
    };

    $scope.getTotalLeaveDaysByBranch = function (branch) {
        var total = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            var brn = $scope.employeesList[i].Branch;
            if (brn === branch) {
                total += ($scope.employeesList[i].LeaveEntitilmentA * 1 + $scope.employeesList[i].LeaveEntitilmentB * 1);
            }
        }
        return total;
    };

    $scope.getTotalLeaveInMonetaryByBranch = function (branch) {
        var totalLeaveInMonitory = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            var brn = $scope.employeesList[i].Branch;
            var EmpSalary = $scope.employeesList[i].salary;
            if (brn === branch) {
                var totalLeave = ($scope.employeesList[i].LeaveEntitilmentA * 1 + $scope.employeesList[i].LeaveEntitilmentB * 1);
                totalLeaveInMonitory += ((EmpSalary * 1) / 24) * (totalLeave * 1);
            }
        }
        return totalLeaveInMonitory;
    };

    $scope.getOverAllTotalLeaveDays = function () {
        var total = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            total = total + ($scope.employeesList[i].LeaveEntitilmentA * 1 + $scope.employeesList[i].LeaveEntitilmentB * 1);
        }
        return total;
    };

    $scope.getOverAllTotalLeaveInMonetary = function () {
        var totalLeaveInMonitory = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            var EmpSalary = $scope.employeesList[i].salary;
            var totalLeave = ($scope.employeesList[i].LeaveEntitilmentA * 1 + $scope.employeesList[i].LeaveEntitilmentB * 1);
            totalLeaveInMonitory += ((EmpSalary * 1) / 24) * (totalLeave * 1);
        }
        return totalLeaveInMonitory;
    };

    $scope.filterReport = function(){
        var startingDate = $scope.startingDate;
        var endingDate = $scope.endingDate;
        $http.post('services/filterandfetchwithjoin.php', {
            starting_Date: startingDate,
            ending_Date: endingDate
        }).then(function (response) { 
           console.log(response.data);
           $scope.LeavesList = response.data;          
           /*$scope.getOverAllTotalLeaveDays = function(){
            var total = 0;
            var leng = $scope.LeavesList.length;
            for (var i = 0; i < leng; i++) {
                total = total + ($scope.LeavesList[i].LeaveEntitilmentA * 1 + $scope.LeavesList[i].LeaveEntitilmentB * 1);
            }
            return total;
          };
           $scope.getOverAllTotalLeaveInMonetary = function(){ 
            var totalLeaveInMonitory = 0;
            var leng = $scope.LeavesList.length;
            for (var i = 0; i < leng; i++) {
                var EmpSalary = $scope.LeavesList[i].salary;
                var totalLeave = ($scope.LeavesList[i].LeaveEntitilmentA * 1 + $scope.LeavesList[i].LeaveEntitilmentB * 1);
                totalLeaveInMonitory += ((EmpSalary * 1) / 24) * (totalLeave * 1);
            }
            return totalLeaveInMonitory;
          };*/      
        });
    };

    $scope.printDiv = function (divId) {
        //Get the HTML of div
        var divElements = document.getElementById(divId).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        window.close();
        window.location.reload();
    };

    $scope.Reset = function(){
        window.location.reload();
    };
});



app.controller('EmployeeReportController', function ($scope, $http, $window, LeaveService, EmployeesService, DepatmentsService, AABranchsServiceWithOutHO, OutlyingBranchsService) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    $scope.todayDate = dd + '/' + mm + '/' + yyyy;

    EmployeesService.saveData().then(function (res) {
        $scope.employeesList = res.data;
    });

    DepatmentsService.saveData().then(function (res) {
        $scope.deps = res.data;
    });

    AABranchsServiceWithOutHO.saveData().then(function (res) {
        $scope.AAbranches = res.data;
    });

    OutlyingBranchsService.saveData().then(function (res) {
        $scope.Outlyingbranches = res.data;
    });

    $scope.getTotalEmployeesByDepartment = function (department) {
        var total = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            var dep = $scope.employeesList[i].Department;
            var branch = $scope.employeesList[i].Branch.toLowerCase();
            if (dep === department && branch === 'head office') {
                total += 1;
            }
        }
        return total;
    };

    $scope.getTotalEmployeesByBranch = function (branch) {
        var total = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            var brn = $scope.employeesList[i].Branch;
            if (brn === branch) {
                total += 1;
            }
        }
        return total;
    };

    $scope.getTotalEmployees = function () {
        var total = 0;
        var leng = $scope.employeesList.length;
        for (var i = 0; i < leng; i++) {
            total += 1;
        }
        return total;
    };

    $scope.getFollowUpDataByDepartment = function () {
        var datas = [];
        $scope.col1 = 0;
        $scope.col2 = 0;
        $scope.col3 = 0;
        $scope.col4 = 0;
        $scope.col5 = 0;
        $scope.col6 = 0;
        $scope.col7 = 0;
        $scope.col8 = 0;
        $scope.totSum2 = 0;

        $http.post('services/FetchData.php', {
            tableName: 'departments'
        }).then(function (response) {
            var length = response.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByDepartment.php', {
                    _departemnt: response.data[i].DepartmentName
                }).then(function (response) {
                    datas.push(response.data);
                    $scope.col1 += response.data[1] * 1;
                    $scope.col2 += response.data[2] * 1;
                    $scope.col3 += response.data[3] * 1;
                    $scope.col4 += response.data[4] * 1;
                    $scope.col5 += response.data[5] * 1;
                    $scope.col6 += response.data[6] * 1;
                    $scope.col7 += response.data[7] * 1;
                    $scope.col8 += response.data[8] * 1;
                    $scope.totSum2 += response.data[1] * 1 + response.data[2] * 1 + response.data[3] * 1 + response.data[4] * 1 + response.data[5] * 1 + response.data[6] * 1 + response.data[7] * 1 + response.data[8] * 1;
                });
            };
        });
        console.log(datas);
        $scope.data1 = datas;
    };

     $scope.getSizeByDepartment = function () {
        $scope.col1 = 0;
        $scope.col2 = 0;
        $scope.col3 = 0;
        $scope.col4 = 0;
        $scope.col5 = 0;
        $scope.col6 = 0;
        $scope.col7 = 0;
        $scope.col8 = 0;
        $scope.male = 0;
        $scope.female = 0;
        $scope.totSum2 = 0;

        $http.post('services/FetchData.php', {
            tableName: 'departments'
        }).then(function (response) {

            var length = response.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByDepartment.php', {
                    _departemnt: response.data[i].DepartmentName
                }).then(function (response) {
                    $scope.col1 += response.data[1] * 1;
                    $scope.col2 += response.data[2] * 1;
                    $scope.col3 += response.data[3] * 1;
                    $scope.col4 += response.data[4] * 1;
                    $scope.col5 += response.data[5] * 1;
                    $scope.col6 += response.data[6] * 1;
                    $scope.col7 += response.data[7] * 1;
                    $scope.col8 += response.data[8] * 1;
                    $scope.totSum2 += response.data[1] * 1 + response.data[2] * 1 + response.data[3] * 1 + response.data[4] * 1 + response.data[5] * 1 + response.data[6] * 1 + response.data[7] * 1 + response.data[8] * 1;
                    $scope.male += response.data[1] * 1 + response.data[3] * 1 + response.data[5] * 1 + response.data[7] * 1;
                    $scope.female += response.data[2] * 1 + response.data[4] * 1 + response.data[6] * 1 + response.data[8] * 1;
                });
            };
         $http.post('services/FetchEmployeesAtHeadOffice.php').then(function (res) {
            $scope.emp18_30 = 0;
            $scope.emp31_50 = 0;
            $scope.emp51 = 0;
            var currYear = new Date().getFullYear();
            var length2 = res.data.length;
            for (let j = 0; j < length2; j++) {
                var bornYear = new Date(res.data[j].date_of_birth).getFullYear();
                console.log(res.data[j].first_name + ' ' + (currYear * 1 - bornYear * 1));
                var empAge = currYear * 1 - bornYear * 1;
                console.log(currYear + ' ' + bornYear + ' ' + empAge);
                if (empAge >= 18 && empAge <= 30) {
                    $scope.emp18_30 += 1;
                }
                else if(empAge >= 31 && empAge <= 50) {
                    $scope.emp31_50 += 1;
                }
                else if(empAge > 50) {
                    $scope.emp51 += 1;
                }
            };
           });
        });
    };

    $scope.getFollowUpDataByBranchType = function () {
        var datas = [];
        $scope.col11 = 0;
        $scope.col22 = 0;
        $scope.col33 = 0;
        $scope.col44 = 0;
        $scope.col55 = 0;
        $scope.col66 = 0;
        $scope.col77 = 0;
        $scope.col88 = 0;
        $scope.totSum1 = 0;

        $http.post('services/FetchAABranchesWithOutHO.php', {
            tableName: 'branches'
        }).then(function (response) {
            var length = response.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByAABranches.php', {
                    branch: response.data[i].BranchName
                }).then(function (response) {
                    datas.push(response.data);
                    $scope.col11 += response.data[1] * 1;
                    $scope.col22 += response.data[2] * 1;
                    $scope.col33 += response.data[3] * 1;
                    $scope.col44 += response.data[4] * 1;
                    $scope.col55 += response.data[5] * 1;
                    $scope.col66 += response.data[6] * 1;
                    $scope.col77 += response.data[7] * 1;
                    $scope.col88 += response.data[8] * 1;
                    $scope.totSum1 += response.data[1] * 1 + response.data[2] * 1 + response.data[3] * 1 + response.data[4] * 1 + response.data[5] * 1 + response.data[6] * 1 + response.data[7] * 1 + response.data[8] * 1;
                });
            }
        });
        console.log(datas);
        $scope.data2 = datas;
    };

     $scope.getSizeByBranchType = function () {
        $scope.col11 = 0;
        $scope.col22 = 0;
        $scope.col33 = 0;
        $scope.col44 = 0;
        $scope.col55 = 0;
        $scope.col66 = 0;
        $scope.col77 = 0;
        $scope.col88 = 0;
        $scope.maleb = 0;
        $scope.femaleb = 0;
        $scope.totSum1 = 0;
        $scope.emp18_30a = 0;
        $scope.emp31_50a = 0;
        $scope.emp51a = 0;
        var currYear = new Date().getFullYear();

        $http.post('services/FetchAABranchesWithOutHO.php', {
            tableName: 'branches'
        }).then(function (respons) {
            var length = respons.data.length;
            for (let i = 0; i < length; i++) {

                $http.post('services/empfollowUpByAABranches.php', {
                    branch: respons.data[i].BranchName
                }).then(function (response) {
                    $scope.col11 += response.data[1] * 1;
                    $scope.col22 += response.data[2] * 1;
                    $scope.col33 += response.data[3] * 1;
                    $scope.col44 += response.data[4] * 1;
                    $scope.col55 += response.data[5] * 1;
                    $scope.col66 += response.data[6] * 1;
                    $scope.col77 += response.data[7] * 1;
                    $scope.col88 += response.data[8] * 1;
                    $scope.totSum1 += response.data[1] * 1 + response.data[2] * 1 + response.data[3] * 1 + response.data[4] * 1 + response.data[5] * 1 + response.data[6] * 1 + response.data[7] * 1 + response.data[8] * 1;
                    $scope.maleb += response.data[1] * 1 + response.data[3] * 1 + response.data[5] * 1 + response.data[7] * 1;
                    $scope.femaleb += response.data[2] * 1 + response.data[4] * 1 + response.data[6] * 1 + response.data[8] * 1;
                });

                $http.post('services/FetchEmployeesOnlyAtAAOROutBranches.php', {
                    branch: respons.data[i].BranchName
                }).then(function (res) {
                var leng = res.data.length;
                if(leng > 0){
                for (let j = 0; j < leng; j++) {
                var bornYear = new Date(res.data[j].date_of_birth).getFullYear();
                console.log(res.data[j].first_name + ' ' + (currYear * 1 - bornYear * 1));
                var empAge = currYear * 1 - bornYear * 1;
                console.log(currYear + ' ' + bornYear + ' ' + empAge + res.data[j].Branch);
                if (empAge >= 18 && empAge <= 30) {
                    $scope.emp18_30a += 1;
                }
                else if(empAge >= 31 && empAge <= 50) {
                    $scope.emp31_50a += 1;
                }
                else if(empAge > 50) {
                    $scope.emp51a += 1;
                }
                }
                }      
            });
          };
        });
    };

    $scope.getSizeByBranchTypeOutlying = function () {
        $scope.col111 = 0;
        $scope.col222 = 0;
        $scope.col333 = 0;
        $scope.col444 = 0;
        $scope.col555 = 0;
        $scope.col666 = 0;
        $scope.col777 = 0;
        $scope.col888 = 0;
        $scope.maleo = 0;
        $scope.femaleo = 0;
        $scope.emp18_30o = 0;
        $scope.emp31_50o = 0;
        $scope.emp51o = 0;
        $scope.totSum = 0;
        var currYear = new Date().getFullYear();

        $http.post('services/FetchOutlyingBranchesOnlly.php', {
            tableName: 'branches'
        }).then(function (respons) {
            var length = respons.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByOutlyingBranches.php', {
                    branch: respons.data[i].BranchName
                }).then(function (response) {
                    $scope.col111 += response.data[1] * 1;
                    $scope.col222 += response.data[2] * 1;
                    $scope.col333 += response.data[3] * 1;
                    $scope.col444 += response.data[4] * 1;
                    $scope.col555 += response.data[5] * 1;
                    $scope.col666 += response.data[6] * 1;
                    $scope.col777 += response.data[7] * 1;
                    $scope.col888 += response.data[8] * 1;
                    $scope.totSum += response.data[1] * 1 + response.data[2] * 1 + response.data[3] * 1 + response.data[4] * 1 + response.data[5] * 1 + response.data[6] * 1 + response.data[7] * 1 + response.data[8] * 1;
                    $scope.maleo += response.data[1] * 1 + response.data[3] * 1 + response.data[5] * 1 + response.data[7] * 1;
                    $scope.femaleo += response.data[2] * 1 + response.data[4] * 1 + response.data[6] * 1 + response.data[8] * 1;
                });

                $http.post('services/FetchEmployeesOnlyAtAAOROutBranches.php', {
                branch: respons.data[i].BranchName
                }).then(function (res) {
                var leng = res.data.length;
                if(leng > 0){
                for (let k = 0; k < leng; k++) {
                var bornYear = new Date(res.data[k].date_of_birth).getFullYear();
                console.log(res.data[k].first_name + ' ' + (currYear * 1 - bornYear * 1));
                var empAge = currYear * 1 - bornYear * 1;
                console.log(currYear + ' ' + bornYear + ' ' + empAge + res.data[k].Branch);
                if (empAge >= 18 && empAge <= 30) {
                    $scope.emp18_30o += 1;
                }
                else if(empAge >= 31 && empAge <= 50) {
                    $scope.emp31_50o += 1;
                }
                else if(empAge > 50) {
                    $scope.emp51o += 1;
                }
                }
                }      
            });
            }
        });
    };


    $scope.getFollowUpDataByBranchTypeOutlying = function () {
        var datas = [];
        $scope.col111 = 0;
        $scope.col222 = 0;
        $scope.col333 = 0;
        $scope.col444 = 0;
        $scope.col555 = 0;
        $scope.col666 = 0;
        $scope.col777 = 0;
        $scope.col888 = 0;
        $scope.totSum = 0;
        $http.post('services/FetchOutlyingBranchesOnlly.php', {
            tableName: 'branches'
        }).then(function (response) {
            var length = response.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByOutlyingBranches.php', {
                    branch: response.data[i].BranchName
                }).then(function (response) {
                    datas.push(response.data);
                    $scope.col111 += response.data[1] * 1;
                    $scope.col222 += response.data[2] * 1;
                    $scope.col333 += response.data[3] * 1;
                    $scope.col444 += response.data[4] * 1;
                    $scope.col555 += response.data[5] * 1;
                    $scope.col666 += response.data[6] * 1;
                    $scope.col777 += response.data[7] * 1;
                    $scope.col888 += response.data[8] * 1;
                    $scope.totSum += response.data[1] * 1 + response.data[2] * 1 + response.data[3] * 1 + response.data[4] * 1 + response.data[5] * 1 + response.data[6] * 1 + response.data[7] * 1 + response.data[8] * 1;
                });
            }
        });
        console.log(datas);
        $scope.data3 = datas;
    };

    $scope.getFollowUpDataByDepartmentBySex = function () {
        var datas = [];
        $scope.col00 = 0;
        $scope.col01 = 0;
        $scope.totalSum = 0;

        $http.post('services/FetchData.php', {
            tableName: 'departments'
        }).then(function (response) {
            var length = response.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByDepartmentBySex.php', {
                    _departemnt: response.data[i].DepartmentName
                }).then(function (response) {
                    datas.push(response.data);
                    $scope.col00 += response.data[1] * 1;
                    $scope.col01 += response.data[2] * 1;
                    $scope.totalSum += response.data[1] * 1 + response.data[2] * 1;
                });
            };
        });
        console.log(datas);
        $scope.data4 = datas;
    };

    $scope.getFollowUpDataByBranchTypeBySex = function () {
        var datas = [];
        $scope.col11 = 0;
        $scope.col22 = 0;
        $scope.totalSum1 = 0;

        $http.post('services/FetchAABranchesWithOutHO.php', {
            tableName: 'branches'
        }).then(function (response) {
            var length = response.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByAABranchesBySex.php', {
                    branch: response.data[i].BranchName
                }).then(function (response) {
                    datas.push(response.data);
                    $scope.col11 += response.data[1] * 1;
                    $scope.col22 += response.data[2] * 1;
                    $scope.totalSum1 += response.data[1] * 1 + response.data[2] * 1;
                });
            }
        });
        console.log(datas);
        $scope.data5 = datas;
    };

    $scope.getFollowUpDataByBranchTypeOutlyingBySex = function () {
        var datas = [];
        $scope.col111 = 0;
        $scope.col222 = 0;
        $scope.totalSum2 = 0;

        $http.post('services/FetchOutlyingBranchesOnlly.php', {
            tableName: 'branches'
        }).then(function (response) {
            var length = response.data.length;
            for (let i = 0; i < length; i++) {
                $http.post('services/empfollowUpByOutlyingBranchesBySex.php', {
                    branch: response.data[i].BranchName
                }).then(function (response) {
                    datas.push(response.data);
                    $scope.col111 += response.data[1] * 1;
                    $scope.col222 += response.data[2] * 1;
                    $scope.totalSum2 += response.data[1] * 1 + response.data[2] * 1;
                });
            }
        });
        console.log(datas);
        $scope.data6 = datas;
    };

    $scope.getEmployeesGenderFollowUpBasedOnOccopation = function () {
        $http.post('services/employeeeFollowUpBasedOnOccopationAndGender.php').
        then(function (response) {
            $scope.colM1 = response.data[0] * 1;
            $scope.colM2 = response.data[1] * 1;
            $scope.colM3 = response.data[2] * 1;
            $scope.colM4 = response.data[3] * 1;
            $scope.colM5 = response.data[4] * 1;
            $scope.colM6 = response.data[5] * 1;
            $scope.colM7 = response.data[6] * 1;
            $scope.colM8 = response.data[7] * 1;
            $scope.colM9 = response.data[8] * 1;
            $scope.colM10 = response.data[9] * 1;

            $scope.colF1 = response.data[10] * 1;
            $scope.colF2 = response.data[11] * 1;
            $scope.colF3 = response.data[12] * 1;
            $scope.colF4 = response.data[13] * 1;
            $scope.colF5 = response.data[14] * 1;
            $scope.colF6 = response.data[15] * 1;
            $scope.colF7 = response.data[16] * 1;
            $scope.colF8 = response.data[17] * 1;
            $scope.colF9 = response.data[18] * 1;
            $scope.colF00 = response.data[19] * 1;

            $scope.total = (response.data[0] * 1) + (response.data[1] * 1) + (response.data[2] * 1) + (response.data[3] * 1) + (response.data[4] * 1) + (response.data[5] * 1) + (response.data[6] * 1) + (response.data[7] * 1) + (response.data[8] * 1) + (response.data[9] * 1) + (response.data[10] * 1) + (response.data[11] * 1) + (response.data[12] * 1) + (response.data[13] * 1) + (response.data[14] * 1) + (response.data[15] * 1) + (response.data[16] * 1) + (response.data[17] * 1) + (response.data[18] * 1) + (response.data[19] * 1);
        });
    };

    $scope.printDiv = function (divId) {
        //Get the HTML of div
        var divElements = document.getElementById(divId).innerHTML;
        //Get the HTML of whole page
        var oldPage = document.body.innerHTML;

        //Reset the page's HTML with div's HTML only
        document.body.innerHTML =
            "<html><head><title></title></head><body>" +
            divElements + "</body>";

        //Print Page
        window.print();

        //Restore orignal HTML
        window.close();
        window.location.reload();
    };


});

function yearCalculator(dateHired) {
    var bthDate, curDate, ageYears, ageMonths, ageWeeks, ageDays, ageText;
    bthDate = new Date(dateHired); // Change the birth date here!
    curDate = new Date();
    if (bthDate > curDate) return;
    ageYears = curDate.getFullYear() - bthDate.getFullYear();
    ageMonths = curDate.getMonth() - bthDate.getMonth();
    ageDays = curDate.getDate() - bthDate.getDate();
    if (ageDays < 0) {
        ageMonths = ageMonths - 1;
        ageDays = ageDays + 31;
    }
    ageWeeks = Math.ceil(ageDays / 7);
    if (ageMonths < 0) {
        ageYears = ageYears - 1;
        ageMonths = ageMonths + 12;
    }
    ageText = "";
    if (ageYears > 0) {
        ageText = ageText + ageYears + " year";
        if (ageYears > 1) ageText = ageText + "s";
    }
    if (ageMonths > 0) {
        if (ageYears > 0) {
            if (ageWeeks > 0) ageText = ageText + ", ";
            else if (ageWeeks == 0) ageText = ageText + " and ";
        }
        ageText = ageText + ageMonths + " month";
        if (ageMonths > 1) ageText = ageText + "s";
    }
    if (ageWeeks > 0) {
        if ((ageYears > 0) || (ageMonths > 0)) ageText = ageText + " and ";
        ageText = ageText + ageWeeks + " week";
        if (ageWeeks > 1) ageText = ageText + "s";
    }
    return ageText;
}


function yearCalculatorUptoCutofdate(dateHired, cutofDate) {
    var bthDate, curDate, ageYears, ageMonths, ageWeeks, ageDays, ageText;
    bthDate = new Date(dateHired); // Change the birth date here!
    curDate = new Date(cutofDate);
    if (bthDate > curDate) return;
    ageYears = curDate.getFullYear() - bthDate.getFullYear();
    ageMonths = curDate.getMonth() - bthDate.getMonth();
    ageDays = curDate.getDate() - bthDate.getDate();
    if (ageDays < 0) {
        ageMonths = ageMonths - 1;
        ageDays = ageDays + 31;
    }
    ageWeeks = Math.ceil(ageDays / 7);
    if (ageMonths < 0) {
        ageYears = ageYears - 1;
        ageMonths = ageMonths + 12;
    }
    ageText = "";
    if (ageYears > 0) {
        ageText = ageText + ageYears + " year";
        if (ageYears > 1) ageText = ageText + "s";
    }
    if (ageMonths > 0) {
        if (ageYears > 0) {
            if (ageWeeks > 0) ageText = ageText + ", ";
            else if (ageWeeks == 0) ageText = ageText + " and ";
        }
        ageText = ageText + ageMonths + " month";
        if (ageMonths > 1) ageText = ageText + "s";
    }
    if (ageWeeks > 0) {
        if ((ageYears > 0) || (ageMonths > 0)) ageText = ageText + " and ";
        ageText = ageText + ageWeeks + " week";
        if (ageWeeks > 1) ageText = ageText + "s";
    }
    return ageText;
}


function Noofmonths(date1, date2) {
    var Nomonths;
    Nomonths = (date2.getFullYear() - date1.getFullYear()) * 12;
    Nomonths -= date1.getMonth() + 1;
    Nomonths += date2.getMonth() + 1; // we should add + 1 to get correct month number
    return Nomonths <= 0 ? 0 : Nomonths;
};

app.factory('EmployeesService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/fetchActiveEmployees.php', {
                tableName: 'employees'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('LeaveService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/fetchwithjoin.php').then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('YearlookUpService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/fetchYears.php').then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('DepatmentsService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/FetchData.php', {
                tableName: 'departments'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('BranchsService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/FetchData.php', {
                tableName: 'branches'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('AABranchsServiceWithOutHO', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/FetchAABranchesWithOutHO.php', {
                tableName: 'branches'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('OutlyingBranchsService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/FetchOutlyingBranchesOnlly.php', {
                tableName: 'branches'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('GradeService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/FetchData.php', {
                tableName: 'grade'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('CerteficationsService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/FetchData.php', {
                tableName: 'certefications'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

app.factory('LeaveTypeService', function ($q, $http) {
    return {
        getData: function () {
            var q = $q.defer();
            $http.post('services/FetchData.php', {
                tableName: 'leavetype'
            }).then(function (response) {
                q.resolve(response);
            }, function (error) {
                q.reject();
            })
            return q.promise;
        },
        saveData: function () {
            return this.getData();
        }
    }
});

function handleFile(e) {
    //Get the files from Upload control
    var files = e.target.files;
    var i, f;
    var rows = [];
    //Loop through files
    for (i = 0, f = files[i]; i != files.length; ++i) {
        var reader = new FileReader();
        var name = f.name;
        reader.onload = function (e) {
            var data = e.target.result;

            var result;
            var workbook = XLSX.read(data, {
                type: 'binary'
            });

            var sheet_name_list = workbook.SheetNames;
            sheet_name_list.forEach(function (y) { /* iterate through sheets */
                //Convert the cell value to Json
                var roa = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                if (roa.length > 0) {
                    result = roa;
                }
            });
            //Get the first column first cell value
            sessionStorage.parsedRows = JSON.stringify(result);
            console.log(result);
            for (let x = 0; x < result.length; x++) {
                var EmpId = result[x].EmpID;
                var Name = result[x].NameofEmployee;
                var Sex = result[x].Sex;
                var DOB = result[x].DateofBirth;
                console.log(DOB);
                var DOE = result[x].DateofEmployment;
                console.log(DOE);
                var EduLevel = result[x].EducationLevel;
                var _Position = result[x].Position;
                var Grade = result[x].Grade;
                var Salary = result[x].Salary;
                var Representation = result[x].Representation;
                var Transport = result[x].Transport;
                var Branch = result[x].Branch;
                var Department = result[x].Department;
                var LeaveEntitlementA = result[x].LeaveEntitlementA;
                var LeaveEntitlementB = result[x].LeaveEntitlementB;
                var ExpiredLeave = result[x].ExpiredLeave;
                var Country = result[x].Country;
                var Nationality = result[x].Nationality;
                var City = result[x].City;
                var SubCity = result[x].SubCity;
                var house_number = result[x].house_number;
                var phone = result[x].phone;
                var email = result[x].email;
                var PerformanceValue = result[x].PerformanceValue;
                var Status = result[x].Status;
                var table = document.getElementById('table');
                var tbody = document.createElement('tbody');
                var tr = document.createElement('tr');
                tr.innerHTML = '<td>' + [EmpId, Name, Sex, DOB, DOE, EduLevel, _Position, Grade, Salary, Representation, Transport, Branch, Department, LeaveEntitlementA, LeaveEntitlementB, ExpiredLeave, Country, Nationality, City, SubCity, house_number, phone, email, PerformanceValue, Status].join('</td><td>') + '</td>';
                tbody.appendChild(tr);
                table.appendChild(tbody);
                //alert(result[0].Name);
            }
        };
        reader.readAsArrayBuffer(f);
    }
    document.getElementById('exportBtn').disabled = false;
};



//To Excel Exporters
var xport = {
    _fallbacktoCSV: true,
    toXLS: function (tableId, filename) {
        this._filename = (typeof filename == 'undefined') ? tableId : filename;

        //var ieVersion = this._getMsieVersion();
        //Fallback to CSV for IE & Edge
        if ((this._getMsieVersion() || this._isFirefox()) && this._fallbacktoCSV) {
            return this.toCSV(tableId);
        } else if (this._getMsieVersion() || this._isFirefox()) {
            alert("Not supported browser");
        }

        //Other Browser can download xls
        var htmltable = document.getElementById(tableId);
        var html = htmltable.outerHTML;

        this._downloadAnchor("data:application/vnd.ms-excel" + encodeURIComponent(html), 'xls');
    },
    toCSV: function (tableId, filename) {
        this._filename = (typeof filename === 'undefined') ? tableId : filename;
        // Generate our CSV string from out HTML Table
        var csv = this._tableToCSV(document.getElementById(tableId));
        // Create a CSV Blob
        var blob = new Blob([csv], {
            type: "text/csv"
        });

        // Determine which approach to take for the download
        if (navigator.msSaveOrOpenBlob) {
            // Works for Internet Explorer and Microsoft Edge
            navigator.msSaveOrOpenBlob(blob, this._filename + ".csv");
        } else {
            this._downloadAnchor(URL.createObjectURL(blob), 'csv');
        }
    },
    _getMsieVersion: function () {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf("MSIE ");
        if (msie > 0) {
            // IE 10 or older => return version number
            return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)), 10);
        }

        var trident = ua.indexOf("Trident/");
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf("rv:");
            return parseInt(ua.substring(rv + 3, ua.indexOf(".", rv)), 10);
        }

        var edge = ua.indexOf("Edge/");
        if (edge > 0) {
            // Edge (IE 12+) => return version number
            return parseInt(ua.substring(edge + 5, ua.indexOf(".", edge)), 10);
        }

        // other browser
        return false;
    },
    _isFirefox: function () {
        if (navigator.userAgent.indexOf("Firefox") > 0) {
            return 1;
        }

        return 0;
    },
    _downloadAnchor: function (content, ext) {
        var anchor = document.createElement("a");
        anchor.style = "display:none !important";
        anchor.id = "downloadanchor";
        document.body.appendChild(anchor);

        // If the [download] attribute is supported, try to use it

        if ("download" in anchor) {
            anchor.download = this._filename + "." + ext;
        }
        anchor.href = content;
        anchor.click();
        anchor.remove();
    },
    _tableToCSV: function (table) {
        // We'll be co-opting `slice` to create arrays
        var slice = Array.prototype.slice;

        return slice
            .call(table.rows)
            .map(function (row) {
                return slice
                    .call(row.cells)
                    .map(function (cell) {
                        return '"t"'.replace("t", cell.textContent);
                    })
                    .join(",");
            })
            .join("\r\n");
    }
};
//End

var modal = document.getElementById('myModal');

// Get the button that opens the modal
var btn = document.getElementById("myBtn");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks the button, open the modal 
btn.onclick = function () {
    modal.style.display = "block";
}

// When the user clicks on <span> (x), close the modal
span.onclick = function () {
    modal.style.display = "none";
}

// When the user clicks anywhere outside of the modal, close it
window.onclick = function (event) {
    if (event.target == modal) {
        modal.style.display = "none";
    }
}